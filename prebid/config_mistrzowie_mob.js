if(g_ads_enabled){
	
	var googletag = null;
	var pbjs = null;

	const PREBID_TIMEOUT = 1200;
	const VIGNETTE_TIMEOUT = 5000;
	const AD_REFRESH_INTERVAL = 10000;

	//div containers

	const recOneCode = 'rec_1';
	const recOne = document.getElementById(recOneCode);

	const recTwoCode = 'rec_2';
	const recTwo = document.getElementById(recTwoCode);

	const recThreeCode = 'rec_3';
	const recThree = document.getElementById(recThreeCode);

	const recFourCode = 'rec_4';
	const recFour = document.getElementById(recFourCode);
	
	const vignette = document.createElement('div');
	vignette.style.textAlign = 'center';
	vignette.style.color = 'grey';
	vignette.style.fontSize = '10px';
	vignette.innerHTML = 'Reklama';

	//google ad slots   
    
    const AAScript = document.createElement('script');
	AAScript.async = true;
    AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');      
    AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');      
    top.document.head.appendChild(AAScript);  

	const GPTScript = document.createElement('script');
	GPTScript.async = true;
	GPTScript.setAttribute('src', '//securepubads.g.doubleclick.net/tag/js/gpt.js');
	top.document.head.appendChild(GPTScript);

	window.googletag = window.googletag || { cmd: [] };

	let slot1, slot2, slot3, slot4;
	googletag.cmd.push(function() {
		// if(recOne){
			slot1 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_336x280_mobile_1', [[336, 280], [300, 250]], recOneCode) //rec_1
            	.addService(googletag.pubads());
		// }
		// if(recTwo){
			slot2 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_336x280_mobile_2', [[336, 280], [300, 250]], recTwoCode) //rec_2
            	.addService(googletag.pubads());
		// }
		// if(recThree){
			slot3 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_336x280_mobile_3', [[336, 280], [300, 250], [320, 480], [320, 180]], recThreeCode) //rec_3
            	.addService(googletag.pubads());
		// }
		// if(recFour){
			slot4 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_336x280_mobile_4', [[336, 280], [300, 250]], recFourCode) //rec_4
            	.addService(googletag.pubads());
		// }

		googletag.pubads().enableSingleRequest();
		googletag.pubads().disableInitialLoad();
		googletag.pubads().collapseEmptyDivs();
		googletag.pubads().setForceSafeFrame(false);
		googletag.pubads().setCentering(true);
		googletag.enableServices();
	});

	googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];

	//prebid currencies

	pbjs = pbjs || {};
	pbjs.que = pbjs.que || [];
		
	// fetch('https://prebid-config-open-data.s3.eu-central-1.amazonaws.com/currencies')
    // .then(resp => resp.json())
    // .then(data => {
    //     console.log("Aktualne kursy walut: ", data);
         
    //     const USD = data.usd.rate;
    //     const EUR = data.eur.rate;
    
        pbjs.bidderSettings = {
			criteo: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
				,
				storageAllowed: true
			}
			,
			adform: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.9;
				}
			}
			,
			smartadserver: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			rtbhouse: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			pulsepoint: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			onedisplay: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.85;
				}
			}
			,
			oftmedia: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.85;
				}
			}
			,
			imonomy: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			rubicon: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.75;
				}
			}
			,
			ix: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			// ,
			// connectad: {
			// 	bidCpmAdjustment: function(bidCpm) {
			// 		return bidCpm;
			// 	}
			// }
			,
			amx: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			visx: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			sspBC: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			adagio: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				},
				storageAllowed: true 
			}
			// ,
			// between: {
			// 	bidCpmAdjustment: function(bidCpm) {
			// 		return bidCpm;
			// 	}
			// } 
			,
			standard: {
				adserverTargeting: [ 
					{
						key: "hb_bidder",
						val: function (bidResponse) {
							return bidResponse.bidderCode;
						}
					}
					,
					{
						key: "hb_adid",
						val: function (bidResponse) {
							return bidResponse.adId;
						}
					}
					,
					{
						key: "hb_pb",
						val: function(bidResponse) {
							const cpm = bidResponse.cpm;
							if (cpm < 10.00) {
								return (Math.floor(cpm * 100) / 100).toFixed(2);
							}
							else {
								return '10.00';
							}
						}
					}
				]
			}
        };
    // });

	//prebid initialization function

	function initAdserver() {
		if (pbjs.initAdserverSet) return;
		pbjs.initAdserverSet = true;
		googletag.cmd.push(function() {
			pbjs.que.push(function() {
				pbjs.setTargetingForGPTAsync();
				googletag.pubads().refresh();
			});
		});
	}

	//ad refresh functions

	function createObserver({interval, element, ratio}) {
		const observer = new IntersectionObserver(entries => {
			handleIntersect({
				entries: entries, 
				interval: interval,
				ratio: ratio
			})
		}, { threshold: 1 });
		observer.observe(element);
	}

	function handleIntersect({entries, interval, ratio}) {
		entries.forEach(entry => {
			interval.clear();
			if(entry.intersectionRatio > ratio.get() && !document.hidden) {
				interval.set();
			};
			ratio.set(entry.intersectionRatio);
		});
	}

	function refreshBid({elementCode, slot}) {
		console.log("refreshAdUnit " + elementCode)
		pbjs.que.push(function() {
			pbjs.requestBids({
				timeout: PREBID_TIMEOUT,
				adUnitCodes: [elementCode],
				bidsBackHandler: function() {
					pbjs.setTargetingForGPTAsync([elementCode]);
					googletag.pubads().refresh([slot]);
					googletag.cmd.push(function() {
						googletag.display(elementCode);
					});
				}
			});
		});
	}

	//adunit refresh inits and settings
	
	//rec_1

	let recOneInterval;
	const recOneIntervalSet = () => {
		recOneInterval = setInterval(() => {
			refreshBid({
				elementCode: recOneCode, 
				slot: slot1
			})
		}, AD_REFRESH_INTERVAL);
	}
	const recOneIntervalClear = () => clearInterval(recOneInterval)

	let recOneRatio = 0;
	const setRecOneRatio = (x) => { recOneRatio = x }
	const getRecOneRatio = () => { return recOneRatio }

	if(recOne){
		window.addEventListener("load", () => {
			createObserver({
				element: recOne,
				interval: {
					set: recOneIntervalSet,
					clear: recOneIntervalClear
				},
				ratio: {
					set: setRecOneRatio,
					get: getRecOneRatio
				}
			});
		}, false);
	}

	//rec_2

	let recTwoInterval;
	const recTwoIntervalSet = () => {
		recTwoInterval = setInterval(() => {
			refreshBid({
				elementCode: recTwoCode, 
				slot: slot2
			})
		}, AD_REFRESH_INTERVAL);
	}
	const recTwoIntervalClear = () => clearInterval(recTwoInterval)

	let recTwoRatio = 0;
	const setRecTwoRatio = (x) => { recTwoRatio = x }
	const getRecTwoRatio = () => { return recTwoRatio }

	if(recTwo){
		window.addEventListener("load", () => {
			createObserver({
				element: recTwo,
				interval: {
					set: recTwoIntervalSet,
					clear: recTwoIntervalClear
				},
				ratio: {
					set: setRecTwoRatio,
					get: getRecTwoRatio
				}
			});
		}, false);
	}

	//rec_3

	let recThreeInterval;
	const recThreeIntervalSet = () => {
		recThreeInterval = setInterval(() => {
			refreshBid({
				elementCode: recThreeCode, 
				slot: slot3
			})
		}, AD_REFRESH_INTERVAL);
	}
	const recThreeIntervalClear = () => clearInterval(recThreeInterval)

	let recThreeRatio = 0;
	const setRecThreeRatio = (x) => { recThreeRatio = x }
	const getRecThreeRatio = () => { return recThreeRatio }

	if(recThree){
		window.addEventListener("load", () => {
			createObserver({
				element: recThree,
				interval: {
					set: recThreeIntervalSet,
					clear: recThreeIntervalClear
				},
				ratio: {
					set: setRecThreeRatio,
					get: getRecThreeRatio
				}
			});
		}, false);
	}

	//rec_4

	let recFourInterval;
	const recFourIntervalSet = () => {
		recFourInterval = setInterval(() => {
			refreshBid({
				elementCode: recFourCode, 
				slot: slot4
			})
		}, AD_REFRESH_INTERVAL);
	}
	const recFourIntervalClear = () => clearInterval(recFourInterval)

	let recFourRatio = 0;
	const setRecFourRatio = (x) => { recFourRatio = x }
	const getRecFourRatio = () => { return recFourRatio }

	if(recFour){
		window.addEventListener("load", () => {
			createObserver({
				element: recFour,
				interval: {
					set: recFourIntervalSet,
					clear: recFourIntervalClear
				},
				ratio: {
					set: setRecFourRatio,
					get: getRecFourRatio
				}
			});
		}, false);
	}

	document.addEventListener("visibilitychange", () => {
		if(document.hidden){
			recOneIntervalClear()
			recTwoIntervalClear()
			recThreeIntervalClear()
			recFourIntervalClear()
		}
	});

	const createVignettes = (g_ads_enabled) => {
		// if(recOne) recOne.prepend(vignette.cloneNode(true));
		// if(recTwo) recTwo.prepend(vignette.cloneNode(true));
		// if(recThree) recThree.prepend(vignette.cloneNode(true));
		// if(recFour) recFour.prepend(vignette.cloneNode(true));

		if(g_ads_enabled){
			const firstAutoAd = document.getElementsByClassName('google-auto-placed')[0];
			const secondAutoAd = document.getElementsByClassName('google-auto-placed')[1];
			const thirdAutoAd = document.getElementsByClassName('google-auto-placed')[2];
			const fourthAutoAd = document.getElementsByClassName('google-auto-placed')[3];
			const fifthAutoAd = document.getElementsByClassName('google-auto-placed')[4];

			if(firstAutoAd) firstAutoAd.prepend(vignette.cloneNode(true));
			if(secondAutoAd) secondAutoAd.prepend(vignette.cloneNode(true));
			if(thirdAutoAd) thirdAutoAd.prepend(vignette.cloneNode(true));
			if(fourthAutoAd) fourthAutoAd.prepend(vignette.cloneNode(true));
			if(fifthAutoAd) fifthAutoAd.prepend(vignette.cloneNode(true));
		}
	}

	setTimeout(() => {
		createVignettes(g_ads_enabled);
	}, VIGNETTE_TIMEOUT);
	   
    //biders
    
	const adUnits = [
		{
			code: recOneCode,
			mediaTypes: {
				banner: {
					sizes: [[336, 280], [300, 250]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '944165'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu', 
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						domain: '//www14.smartadserver.com', 
						siteId: 145384, 
						pageId: 770176, 
						formatId: 52161
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						domain: '//prg.smartadserver.com',
						siteId: 162117,
						pageId: 996330,
						formatId: 54226
					}
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047', siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId:'1624184',
						sizes:'16',
					}
				}
				,
				{
					bidder: 'visx',
					params: {
						uid: '914976'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: recOneCode,
						placement: recOneCode,
						environment: 'mobile',
					}
				}   
				,
				{
					bidder: 'sspBC'
				}       
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				//         placementId: "122108155248577"
				//     }
				// } 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'mubo8g7fqZ',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		}
		,
		{
			code: recTwoCode,
			mediaTypes: {
				banner: {
					sizes: [[336, 280], [300, 250]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '944166'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu', 
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						domain: '//www14.smartadserver.com', 
						siteId: 145384, 
						pageId: 770176, 
						formatId: 52161
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						domain: '//prg.smartadserver.com', 
						siteId: 162117, 
						pageId: 996334, 
						formatId: 54302
					}
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047', 
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId:'1624184',
						sizes:'16'
					}
				}   
				,
				{
					bidder: 'visx',
					params: {
						uid: '914977'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: recTwoCode,
						placement: recTwoCode,
						environment: 'mobile',
					}
				} 
				,
				{
					bidder: 'sspBC'
				}  
				,
				{
				    bidder: 'adpone',
				    params: {
				        placementId: "122108155248577"
				    }
				} 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'mubo8g7fqZ',
						supplyType: "site"
				    }
				}
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		}
		,
		{
			code: recThreeCode,
			mediaTypes: {
				banner: {
					sizes: [[336, 280], [300, 250], [320, 180], [320, 480]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '944167'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu', 
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						domain: '//www14.smartadserver.com', 
						siteId: 145384, 
						pageId: 770177, 
						formatId: 52161
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						domain: '//prg.smartadserver.com', 
						siteId: 162117, 
						pageId: 996335, 
						formatId: 54303
					}
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047', 
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId:'1624184',
						sizes:'16'
					}
				}
				,
				{
					bidder: 'visx',
					params: {
						uid: '914978'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: recThreeCode,
						placement: recThreeCode,
						environment: 'mobile'
					}
				} 
				,
				{
					bidder: 'sspBC'
				} 
				// ,
				// {
				//     bidder: 'adpone',
				//     params:{
				//         placementId: "122108155248577"
				//     }
				// } 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'mubo8g7fqZ',
						supplyType: "site"
				    }
				}
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		}
		,
		{
			code: recFourCode,
			mediaTypes: {
				banner: {
					sizes: [[336, 280], [300, 250]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '944168'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu', 
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						domain: '//www14.smartadserver.com', 
						siteId: 145384, 
						pageId: 770177, 
						formatId: 52161
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						domain: '//prg.smartadserver.com', 
						siteId: 162117, 
						pageId: 996336, 
						formatId: 54304
					}
				}
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047', 
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId:'1624184',
						sizes:'16',
					}
				}
				,
				{
					bidder: 'visx',
					params: {
						uid: '914979'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: recFourCode,
						placement: recFourCode,
						environment: 'mobile'
					}
				} 
				,
				{
					bidder: 'sspBC'
				}  
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				//         placementId: "122108155248577"
				//     }
				// }  
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'mubo8g7fqZ',
						supplyType: "site"
				    }
				}  
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// }     
			]
		}    
	];

	pbjs = pbjs || {};
	pbjs.que = pbjs.que || [];

	pbjs.que.push(function() {
		pbjs.addAdUnits(adUnits);
		pbjs.setConfig({
			userSync: {
				syncEnabled: true,
				filterSettings: {        
					all: {        
						bidders: "*",        
						filter: "include"
					}        
				},        
				syncsPerBidder: 5,        
				syncDelay: 3000,        
				auctionDelay: 0,        
				userIds: [
					{                
						name: "criteo",
					}
				]
			},
			schain: { 
				validation: "strict", 
				config: { 
					ver: "1.0", 
					complete: 1, 
					nodes: [
						{ 
							asi: "yieldriser.com", 
							sid: "27", 
							hp: 1 
						}
					]
				}
			},
			bidderSequence: "random",
			disableAjaxTimeout: true,
			consentManagement: {
				cmpApi: 'iab',
				timeout: 5000,
				allowAuctionWithoutConsent: true
			},
			criteo: {
				storageAllowed: true, // opt-in to allow Criteo bid adapter to access the storage
			},
			currency: {
				adServerCurrency: "PLN",
			 }
		});
		pbjs.requestBids({
			bidsBackHandler: initAdserver,
			timeout: PREBID_TIMEOUT
		});
	});
}