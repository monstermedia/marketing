if (g_ads_enabled && MMG.isAdblockDisabled()) {

    console.log("demotywatory_react_art_config");
    
    const AAScript = document.createElement('script');
	AAScript.async = true;
    AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');
    AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');
    top.document.head.appendChild(AAScript);
    
    // GOOGLE TAG INIT
    var googletag = window.googletag || { cmd: [] };
    googletag.cmd = googletag.cmd || [];
    googletag.cmd.push(function() {
        //googletag.pubads().disableInitialLoad();
        googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs();
        googletag.pubads().setCentering(true);
    });
    console.log("googletag loaded: " + googletag._loaded_);
    
    // PREBID INIT
    var pbjs = window.pbjs || {};
    pbjs.que = pbjs.que || [];
    console.log("prebid loaded: " + pbjs.libLoaded);
    
    
    // SLOTS
    const topBillInnerArt = 'top_bill_inner';
    const topRectInnerArt = 'top_rect_inner';
    const bottomRectInnerArt = 'bottom_rect_inner';
    
    
    // ADAPTERS  
    const adUnits=[
            {
            code: topBillInnerArt,
            mediaTypes: {
                banner: {
                    sizes: [[300, 250],[336, 280]],
                }
            },
            bids: [ 
                {
                    bidder: 'adform',
                    params: {
                        mid: '1305468'
                    }
                }
                ,
                {
                    bidder: 'smartadserver',
                    params: {
                        networkId: 2422, siteId: 170496, pageId: 840781, formatId: 52161,
                    }
                }
                ,
                {
                    bidder: 'smartadserver',
                    params: {
                        networkId: 2581, siteId: 167130, pageId: 907607, formatId: 54302,
                    }
                },
                {
                    bidder: 'rubicon',
                        params: {
                            accountId: '21594',
                            siteId: '406510',
                            zoneId:'2281136',
                            sizes:'16',
                        }
                }
            ,
            {
                bidder: 'criteo',
                params: {
                    zoneId: 1418180,
                    publisherSubId: topBillInnerArt
                }
            }
            ,
        
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu',
                    publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            } 
            ,
            {
                bidder: 'sspBC'
            }  
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 23981220
                }
            }   
            ,
            {
                bidder: 'connectad',
                params: {
                   networkId: '10047',
                   siteId: '1055768'
                }
            }
            ,
            {
                bidder: 'adagio',
                params:{
                        organizationId: '1120',
                        site: 'demotywatory-pl',
                        adUnitElementId: topBillInnerArt,
                        placement: topBillInnerArt,
                        environment: 'mobile',
                        }
                    }

        ]
        }  
        ,
        {
            code: topRectInnerArt,
            mediaTypes: {
                banner: {
                    sizes: [[300, 250],[336, 280]],
                }
            }
            ,
            bids: [ {
                bidder: 'adform',
                params: {
                    mid: '344424'
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2422, siteId:  170496, pageId: 840780, formatId: 62779,
                }
            }
            ,
            {
                bidder: 'smartadserver',
                params: {
                    networkId: 2581, siteId:  167130, pageId: 907610, formatId: 54303,
                }
            }
            ,
            {
                bidder: 'sspBC'
            } 
            ,
            {
                bidder: 'rubicon',
                    params: {
                        accountId: '21594',
                        siteId: '406510',
                        zoneId:'2281136',
                        sizes:'16',
                }
            } 
            ,
            {
                bidder: 'criteo',
                params: {
                    zoneId: 1418180,
                    publisherSubId: topRectInnerArt
                }
            } 
            ,
        
            {
                bidder: "rtbhouse",
                params: {
                    region: 'prebid-eu',
                    publisherId: 'ubZbp6DokIAsAJBBqd3T'
                }
            } 
            ,
            {
                bidder: 'oftmedia',
                params: {
                    placementId: 23981220
                }
            }  
            ,
            {
                bidder: 'connectad',
                params: {
                   networkId: '10047',
                   siteId: '1055768'
                }
            }
            ,
            {
                bidder: 'adagio',
                params:{
                        organizationId: '1120',
                        site: 'demotywatory-pl',
                        adUnitElementId: topRectInnerArt,
                        placement: topRectInnerArt,
                        environment: 'mobile',
                        }
                    }

        ]
        } 
            ,
            {
                code: 'bottom_rect_inner',
                mediaTypes: {
                    banner: {
                        sizes: [[300, 250],[336, 280]],
                    }
            }
                ,
                bids: [ {
                    bidder: 'adform',
                    params: {
                        mid: '344427'
                    }
                }
                ,
                {
                    bidder: 'smartadserver',
                    params: {
                        networkId: 2422, siteId:  154887, pageId: 792814, formatId: 52161,
                    }
                }
                ,
                {
                    bidder: 'smartadserver',
                    params: {
                        networkId: 2581, siteId:  167130, pageId: 1045260, formatId: 54306,
                    }
                }
                ,
                {
                    bidder: 'sspBC'
                } 
                ,
                {
                    bidder: 'rubicon',
                        params: {
                            accountId: '21594',
                            siteId: '406510',
                            zoneId:'2281136',
                            sizes:'16',
                    }
                }
                ,
                {
                    bidder: 'criteo',
                    params: {
                        zoneId: 1418180,
                        publisherSubId: bottomRectInnerArt
                    }
                } 
                ,
        
                {
                    bidder: "rtbhouse",
                    params: {
                        region: 'prebid-eu',
                        publisherId: 'ubZbp6DokIAsAJBBqd3T'
                    }
                }  
                ,
                {
                    bidder: 'oftmedia',
                    params: {
                        placementId: 23981220
                    }
                }  
                ,
                {
                    bidder: 'connectad',
                    params: {
                       networkId: '10047',
                       siteId: '1055768'
                    }
                }
                ,
                {
                    bidder: 'adagio',
                    params:{
                            organizationId: '1120',
                            site: 'demotywatory-pl',
                            adUnitElementId: bottomRectInnerArt,
                            placement: bottomRectInnerArt,
                            environment: 'mobile',
                            }
                        }
              
            
            ]
        }          
        
    ];
    
    // waluty, przeliczenia, buckety
    
    fetch('https://prebid-config-open-data.s3.eu-central-1.amazonaws.com/currencies')
    .then(resp => resp.json())
    .then(data => {
        console.log("Aktualne kursy walut: ", data);
         
        const USD = data.usd.rate;
        const EUR = data.eur.rate;
    
        pbjs.bidderSettings = {
        criteo: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*EUR;
            }
            ,
            storageAllowed: true
        }
        ,
        adform: {
            bidCpmAdjustment: function(bidCpm) {
                return (bidCpm*0.9)*EUR;
            }
        }
        ,
        smartadserver: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm;
            }
        }
        ,
        rtbhouse: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        pulsepoint: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        onedisplay: {
            bidCpmAdjustment: function(bidCpm) {
                return (bidCpm*0.85)*EUR;
            }
        }
        ,
        oftmedia: {
            bidCpmAdjustment: function(bidCpm) {
                return (bidCpm*0.85)*USD;
            }
        }
        ,
        imonomy: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        rubicon: {
            bidCpmAdjustment: function(bidCpm) {
                return (bidCpm*0.75)*USD;
            }
        }
        ,
        ix: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        connectad: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        }
        ,
        amx: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm;
            }
        }
        ,
        visx: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm;
            }
        }
                ,
        adagio: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            },
            storageAllowed: true 
        }
        ,
        between: {
            bidCpmAdjustment: function(bidCpm) {
                return bidCpm*USD;
            }
        } 
        ,
        standard: {
            adserverTargeting: [ {
                key: "hb_bidder",
                val: function (bidResponse) {
                    return bidResponse.bidderCode;
                }
            }
            ,
            {
                key: "hb_adid",
                val: function (bidResponse) {
                    return bidResponse.adId;
                }
            }
            ,
            {
                key: "hb_pb",
                val: function(bidResponse) {
                    var cpm=bidResponse.cpm;
                    if (cpm < 10.00) {
                        return (Math.floor(cpm * 100) / 100).toFixed(2);
                    }
                    else {
                        return '10.00';
                    }
                }
            }
            ]
        }
        };
    });
    
    // TWORZENIE WINIET W JEDNOSTKACH
    setTimeout(() => {
        if ((document.getElementsByClassName('google-auto-placed')[0]) !== undefined){
            document.getElementsByClassName('google-auto-placed')[0].prepend(divClone);
            divClone = div.cloneNode(true);
        } else
        {void(0);}
            
        if ((document.getElementsByClassName('google-auto-placed')[1]) !== undefined){
            document.getElementsByClassName('google-auto-placed')[1].prepend(divClone);
            divClone = div.cloneNode(true);
        } else
        {void(0);}
            
        if ((document.getElementsByClassName('google-auto-placed')[2]) !== undefined){
            document.getElementsByClassName('google-auto-placed')[2].prepend(divClone);    
            divClone = div.cloneNode(true);
        } else
        {void(0);}
            
        if ((document.getElementsByClassName('google-auto-placed')[3]) !== undefined){
            document.getElementsByClassName('google-auto-placed')[3].prepend(divClone);
            divClone = div.cloneNode(true);
        } else
        {void(0);}
            
        if ((document.getElementsByClassName('google-auto-placed')[4]) !== undefined){
            document.getElementsByClassName('google-auto-placed')[4].prepend(divClone);
            divClone = div.cloneNode(true);
        } else
        {void(0);}
            
        if ((document.getElementsByClassName('google-auto-placed')[5]) !== undefined){
            document.getElementsByClassName('google-auto-placed')[5].prepend(divClone);
            divClone = div.cloneNode(true);
        } else
        {void(0);}
    }, 5000);
    
    // Adserver INIT
    function initAdserver() {
        if (pbjs.initAdserverSet) return;
        pbjs.initAdserverSet = true;
        googletag.cmd.push(function() {
            pbjs.que.push(function() {
                pbjs.setTargetingForGPTAsync();
                googletag.pubads().refresh();
            });
        });
    }
    
    // PREBID FUNCTIONS
    pbjs.que.push(function() {
        pbjs.adUnits = [];
        pbjs.addAdUnits(adUnits);
        pbjs.setConfig({
            userSync: {
                userIds: [{
                    name: "criteo"
                }]
            },
            "schain":{ 
                "validation": "strict", 
                "config": { 
                    "ver": "1.0", 
                    "complete": 1, 
                    "nodes": [{ 
                        "asi": "yieldriser.com", 
                        "sid": "27", 
                        "hp": 1
                    }]
                }
            }
            ,
            bidderSequence: "random",
            consentManagement: {
                cmpApi: 'iab',
                timeout: 5000,
                allowAuctionWithoutConsent: true
            }
        });
        pbjs.requestBids({
            bidsBackHandler: initAdserver,
            timeout: 1200
        });
    });
    
    console.log(googletag);
    console.log(pbjs);
    }