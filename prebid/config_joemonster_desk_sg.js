if(g_ads_enabled){
	
	var googletag = null;
	var pbjs = null;

	const PREBID_TIMEOUT = 3000;
	const VIGNETTE_TIMEOUT = 5000;
	const AD_REFRESH_INTERVAL = 10000;

	//div containers

	const topBill = document.getElementById('main_billboard');
	const topBillInnerCode = 'top_bill_inner';
	const topBillInner = document.getElementById(topBillInnerCode);

	const leftSky = document.getElementById('leftSky');
	const leftSkyInnerCode = 'left_Sky_inner';
	const leftSkyInner = document.getElementById(leftSkyInnerCode);

	const rightSky = document.getElementById('rightSky');
	const rightSkyInnerCode = 'right_Sky_inner';
	const rightSkyInner = document.getElementById(rightSkyInnerCode);

	const rightRect = document.getElementById('rightRect');
	const rightRectInnerCode = 'right_rect_inner';
	const rightRectInner = document.getElementById(rightRectInnerCode);

	const middleRect = document.getElementById('middleRect');
	const middleRectInnerCode = 'middle_rect_inner';
	const middleRectInner = document.getElementById(middleRectInnerCode);
	
	const bottomRect = document.getElementById('bottomRect');
	const bottomRectInnerCode = 'bottom_rect_inner';
	const bottomRectInner = document.getElementById(bottomRectInnerCode);
	
	const vignette = document.createElement('div');
	vignette.style.textAlign = 'center';
	vignette.style.color = 'grey';
	vignette.style.fontSize = '10px';
	vignette.innerHTML = 'Reklama';

	//google ad slots   
    
    const AAScript = document.createElement('script');
	AAScript.async = true;
    AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');      
    AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');      
    top.document.head.appendChild(AAScript);  

	const GPTScript = document.createElement('script');
	GPTScript.async = true;
	GPTScript.setAttribute('src', '//securepubads.g.doubleclick.net/tag/js/gpt.js');
	top.document.head.appendChild(GPTScript);

	window.googletag = window.googletag || { cmd: [] };

	let slot1, slot2, slot3, slot4, slot5, slot6;
	googletag.cmd.push(function() {
		//if(topBillInner){
			slot1 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/9588257334/joemonster.org_top_bill', [[750, 200],[750, 100],[728, 90]], topBillInnerCode) //top_bill_inner
                .addService(googletag.pubads());
		//}
		//if(leftSkyInner){
			slot2 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/9588257334/joemonster.org_left_Sky', [[160, 600]], leftSkyInnerCode) //left_Sky_inner
                .addService(googletag.pubads());
		//}
		//if(rightSkyInner){
			slot3 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/9588257334/joemonster.org_right_Sky', [[300, 600]], rightSkyInnerCode) //right_Sky_inner
                .addService(googletag.pubads());
		//}
		//if(rightRectInner){
			slot4 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/9588257334/joemonster.org_right_rect', [[300, 250]], rightRectInnerCode) //right_rect_inner
                .addService(googletag.pubads());
		//}
		//if(middleRectInner){
			slot5 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/9588257334/joemonster.org_middle_rect', [[300, 250],[336, 280]], middleRectInnerCode) //middle_rect_inner
                .addService(googletag.pubads());
		//}
		//if(bottomRectInner){
			slot6 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/9588257334/joemonster.org_bottom_rect', [[300, 250],[336, 280]], bottomRectInnerCode) //bottom_rect_inner
                .addService(googletag.pubads());
		//}

		googletag.pubads().enableSingleRequest();
		googletag.pubads().disableInitialLoad();
		googletag.pubads().collapseEmptyDivs();
		googletag.pubads().setForceSafeFrame(false);
		googletag.pubads().setCentering(true);
		googletag.enableServices();
	});

	googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];

	//prebid currencies

	pbjs = pbjs || {};
	pbjs.que = pbjs.que || [];
		
	// fetch('https://prebid-config-open-data.s3.eu-central-1.amazonaws.com/currencies')
    // .then(resp => resp.json())
    // .then(data => {
    //     console.log("Aktualne kursy walut: ", data);
         
    //     const USD = data.usd.rate;
    //     const EUR = data.eur.rate;
    
        pbjs.bidderSettings = {
            criteo: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm;
                }
                ,
                storageAllowed: true
            }
            ,
            adform: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm*0.9;
                }
            }
            ,
            smartadserver: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm;
                }
            }
            ,
            rtbhouse: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm;
                }
            }
            ,
            pulsepoint: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm;
                }
            }
            ,
            onedisplay: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm*0.85;
                }
            }
            ,
            oftmedia: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm*0.85;
                }
            }
            ,
            imonomy: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm;
                }
            }
            ,
            rubicon: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm*0.75;
                }
            }
            ,
            ix: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm;
                }
            }
            // ,
            // connectad: {
            //     bidCpmAdjustment: function(bidCpm) {
            //         return bidCpm;
            //     }
            // }
            ,
            amx: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm;
                }
            }
            ,
            adagio: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm;
                },
                storageAllowed: true 
            }
            ,
            between: {
                bidCpmAdjustment: function(bidCpm) {
                    return bidCpm;
                }
            } 
            ,
            standard: {
                adserverTargeting: [
					{
						key: "hb_bidder",
						val: function (bidResponse) {
							return bidResponse.bidderCode;
						}
					}
					,
					{
						key: "hb_adid",
						val: function (bidResponse) {
							return bidResponse.adId;
						}
					}
					,
					{
						key: "hb_pb",
						val: function(bidResponse) {
							const cpm = bidResponse.cpm;
							if (cpm < 10.00) {
								return (Math.floor(cpm * 100) / 100).toFixed(2);
							}
							else {
								return '10.00';
							}
						}
					}
                ]
            }
        };
    // });

	//prebid initialization function

	function initAdserver() {
		if (pbjs.initAdserverSet) return;
		pbjs.initAdserverSet = true;
		googletag.cmd.push(function() {
			pbjs.que.push(function() {
				pbjs.setTargetingForGPTAsync();
				googletag.pubads().refresh();
			});
		});
	}

	//ad refresh functions

	function createObserver({interval, element, ratio}) {
		const observer = new IntersectionObserver(entries => {
			handleIntersect({
				entries: entries, 
				interval: interval,
				ratio: ratio
			})
		}, { threshold: 1 });
		observer.observe(element);
	}

	function handleIntersect({entries, interval, ratio}) {
		entries.forEach(entry => {
			interval.clear();
			if(entry.intersectionRatio > ratio.get() && !document.hidden) {
				interval.set();
			};
			ratio.set(entry.intersectionRatio);
		});
	}

	function refreshBid({elementCode, slot}) {
		console.log("refreshAdUnit " + elementCode)
		pbjs.que.push(function() {
			pbjs.requestBids({
				timeout: PREBID_TIMEOUT,
				adUnitCodes: [elementCode],
				bidsBackHandler: function() {
					pbjs.setTargetingForGPTAsync([elementCode]);
					googletag.pubads().refresh([slot]);
					googletag.cmd.push(function() {
						googletag.display(elementCode);
					});
				}
			});
		});
	}

	//adunit refresh inits and settings

	//top_bill_inner

	let topBillInnerInterval;
	const topBillInnerIntervalSet = () => {
		topBillInnerInterval = setInterval(() => {
			refreshBid({
				elementCode: topBillInnerCode, 
				slot: slot1
			})
		}, AD_REFRESH_INTERVAL);
	}
	const topBillInnerIntervalClear = () => clearInterval(topBillInnerInterval)

	let topBillInnerRatio = 0;
	const setTopBillInnerRatio = (x) => { topBillInnerRatio = x }
	const getTopBillInnerRatio = () => { return topBillInnerRatio }

	if(topBillInner){
		window.addEventListener("load", () => {
			createObserver({
				element: topBillInner,
				interval: {
					set: topBillInnerIntervalSet,
					clear: topBillInnerIntervalClear
				},
				ratio: {
					set: setTopBillInnerRatio,
					get: getTopBillInnerRatio
				}
			});
		}, false);
	}

	//left_Sky_inner

	let leftSkyInnerInterval;
	const leftSkyInnerIntervalSet = () => {
		leftSkyInnerInterval = setInterval(() => {
			refreshBid({
				elementCode: leftSkyInnerCode, 
				slot: slot2
			})
		}, AD_REFRESH_INTERVAL);
	}
	const leftSkyInnerIntervalClear = () => clearInterval(leftSkyInnerInterval)

	let leftSkyInnerRatio = 0;
	const setLeftSkyInnerRatio = (x) => { leftSkyInnerRatio = x }
	const getLeftSkyInnerRatio = () => { return leftSkyInnerRatio }

	if(leftSkyInner){
		window.addEventListener("load", () => {
			createObserver({
				element: leftSkyInner,
				interval: {
					set: leftSkyInnerIntervalSet,
					clear: leftSkyInnerIntervalClear

				},
				ratio: {
					set: setLeftSkyInnerRatio,
					get: getLeftSkyInnerRatio
				}
			});
		}, false);
	}

	//right_Sky_inner

	let rightSkyInnerInterval;
	const rightSkyInnerIntervalSet = () => {
		rightSkyInnerInterval = setInterval(() => {
			refreshBid({
				elementCode: rightSkyInnerCode, 
				slot: slot3
			})
		}, AD_REFRESH_INTERVAL);
	}
	const rightSkyInnerIntervalClear = () => clearInterval(rightSkyInnerInterval)

	let rightSkyInnerRatio = 0;
	const setRightSkyInnerRatio = (x) => { rightSkyInnerRatio = x }
	const getRightSkyInnerRatio = () => { return rightSkyInnerRatio }

	if(rightSkyInner){
		window.addEventListener("load", () => {
			createObserver({
				element: rightSkyInner,
				interval: {
					set: rightSkyInnerIntervalSet,
					clear: rightSkyInnerIntervalClear
				},
				ratio: {
					set: setRightSkyInnerRatio,
					get: getRightSkyInnerRatio
				}
			});
		}, false);
	}
	
	//right_rect_inner

	let rightRectInnerInterval;
	const rightRectInnerIntervalSet = () => {
		rightRectInnerInterval = setInterval(() => {
			refreshBid({
				elementCode: rightRectInnerCode, 
				slot: slot4
			})
		}, AD_REFRESH_INTERVAL);
	}
	const rightRectInnerIntervalClear = () => clearInterval(rightRectInnerInterval)

	let rightRectInnerRatio = 0;
	const setRightRectInnerRatio = (x) => { rightRectInnerRatio = x }
	const getRightRectInnerRatio = () => { return rightRectInnerRatio }

	if(rightRectInner){
		window.addEventListener("load", () => {
			createObserver({
				element: rightRectInner,
				interval: {
					set: rightRectInnerIntervalSet,
					clear: rightRectInnerIntervalClear
				},
				ratio: {
					set: setRightRectInnerRatio,
					get: getRightRectInnerRatio
				}
			});
		}, false);
	}

	//middle_rect_inner

	let middleRectInnerInterval;
	const middleRectInnerIntervalSet = () => {
		middleRectInnerInterval = setInterval(() => {
			refreshBid({
				elementCode: middleRectInnerCode, 
				slot: slot5
			})
		}, AD_REFRESH_INTERVAL);
	}
	const middleRectInnerIntervalClear = () => clearInterval(middleRectInnerInterval)

	let middleRectInnerRatio = 0;
	const setMiddleRectInnerRatio = (x) => { middleRectInnerRatio = x }
	const getMiddleRectInnerRatio = () => { return middleRectInnerRatio }

	if(middleRectInner){
		window.addEventListener("load", () => {
			createObserver({
				element: middleRectInner,
				interval: {
					set: middleRectInnerIntervalSet,
					clear: middleRectInnerIntervalClear
				},
				ratio: {
					set: setMiddleRectInnerRatio,
					get: getMiddleRectInnerRatio
				}
			});
		}, false);
	}

	//bottom_rect_inner

	let bottomRectInnerInterval;
	const bottomRectInnerIntervalSet = () => {
		bottomRectInnerInterval = setInterval(() => {
			refreshBid({
				elementCode: bottomRectInnerCode, 
				slot: slot6
			})
		}, AD_REFRESH_INTERVAL);
	}
	const bottomRectInnerIntervalClear = () => clearInterval(bottomRectInnerInterval)

	let bottomRectInnerRatio = 0;
	const setBottomRectInnerRatio = (x) => { bottomRectInnerRatio = x }
	const getBottomRectInnerRatio = () => { return bottomRectInnerRatio }

	if(bottomRectInner){
		window.addEventListener("load", () => {
			createObserver({
				element: bottomRectInner,
				interval: {
					set: bottomRectInnerIntervalSet,
					clear: bottomRectInnerIntervalClear
				},
				ratio: {
					set: setBottomRectInnerRatio,
					get: getBottomRectInnerRatio
				}
			});
		}, false);
	}

	document.addEventListener("visibilitychange", () => {
		if(document.hidden){
			topBillInnerIntervalClear()
			leftSkyInnerIntervalClear()
			rightSkyInnerIntervalClear()
			rightRectInnerIntervalClear()
			middleRectInnerIntervalClear()
			bottomRectInnerIntervalClear()
		}
	});

	const createVignettes = (g_ads_enabled) => {
		//if(topBill) topBill.prepend(vignette.cloneNode(true));
		//if(leftSky) leftSky.prepend(vignette.cloneNode(true));
		//if(rightSky) rightSky.prepend(vignette.cloneNode(true));
		//if(rightRect) rightRect.prepend(vignette.cloneNode(true));
		//if(middleRect) middleRect.prepend(vignette.cloneNode(true));
		//if(bottomRect) bottomRect.prepend(vignette.cloneNode(true));
		
		const topBill = document.getElementById('main_billboard').prepend(vignette.cloneNode(true));
		const leftSky = document.getElementById('leftSky').prepend(vignette.cloneNode(true));
		const rightSky = document.getElementById('rightSky').prepend(vignette.cloneNode(true));
		const rightRect = document.getElementById('rightRect').prepend(vignette.cloneNode(true));
		const middleRect = document.getElementById('middleRect').prepend(vignette.cloneNode(true));
		const bottomRect = document.getElementById('bottomRect').prepend(vignette.cloneNode(true));

		if(g_ads_enabled){
			const firstAutoAd = document.getElementsByClassName('google-auto-placed')[0];
			const secondAutoAd = document.getElementsByClassName('google-auto-placed')[1];
			const thirdAutoAd = document.getElementsByClassName('google-auto-placed')[2];
			const fourthAutoAd = document.getElementsByClassName('google-auto-placed')[3];
			const fifthAutoAd = document.getElementsByClassName('google-auto-placed')[4];
			const sixthAutoAd = document.getElementsByClassName('google-auto-placed')[5];
			const seventhAutoAd = document.getElementsByClassName('google-auto-placed')[6];

			if(firstAutoAd) firstAutoAd.prepend(vignette.cloneNode(true));
			if(secondAutoAd) secondAutoAd.prepend(vignette.cloneNode(true));
			if(thirdAutoAd) thirdAutoAd.prepend(vignette.cloneNode(true));
			if(fourthAutoAd) fourthAutoAd.prepend(vignette.cloneNode(true));
			if(fifthAutoAd) fifthAutoAd.prepend(vignette.cloneNode(true));
			if(sixthAutoAd) sixthAutoAd.prepend(vignette.cloneNode(true));
			if(seventhAutoAd) seventhAutoAd.prepend(vignette.cloneNode(true));
		}
	}

	setTimeout(() => {
		createVignettes(g_ads_enabled);
	}, VIGNETTE_TIMEOUT);
	   
    //biders
    
	const adUnits = [
		{
			code: topBillInnerCode,
			mediaTypes: {
				banner: {
					sizes: [[750, 200],[750, 100],[728, 90]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '90242'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 154914,
						pageId: 930696,
						formatId: 51751
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 167862,
						pageId: 930652,
						formatId: 56391
					}
				}
				,
				{
					bidder: 'sspBC'
				}  
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '396252',
						zoneId: '2212972',
						sizes: '40'
					}
				}   
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418179,
						publisherSubId: topBillInnerCode
					}
				} 
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 23318777
					}
				}  
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1054371'
				// 	}
				// }   
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'joemonster-org',
						adUnitElementId: topBillInnerCode,
						placement: topBillInnerCode,
						environment: 'desktop'
					}
				}
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				//         placementId: "122108155714681"
				//     }
				// } 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'Gyy56HczSY',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		}
		,
		{
			code: leftSkyInnerCode,
			mediaTypes: {
				banner: {
					sizes: [[160, 600]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '139548'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 154914,
						pageId: 930695,
						formatId: 53517
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 167862,
						pageId: 930642,
						formatId: 55373
					}
				}
				,
				{
					bidder: 'sspBC'
				}  
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '396252',
						zoneId: '2212964',
						sizes: '9'
					}
				}   
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1421652,
						publisherSubId: leftSkyInnerCode
					}
				} 
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 23318777
					}
				}  
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1054371'
				// 	}
				// }   
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'joemonster-org',
						adUnitElementId: leftSkyInnerCode,
						placement: leftSkyInnerCode,
						environment: 'desktop'
					}
				}
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				//         placementId: "12210815576667"
				//     }
				// } 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'oTCp7L7Cfx',
						supplyType: "site"
				    }
				} 	
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		}  
		,
		{
			code: rightRectInnerCode,
			mediaTypes: {
				banner: {
					sizes: [[300, 250]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '414599'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 154914,
						pageId: 930697,
						formatId: 52161
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 167862,
						pageId: 930653,
						formatId: 54226
					}
				}
				,
				{
					bidder: 'sspBC'
				}  
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '396252',
						zoneId: '2212970',
						sizes: '16'
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418175,
						publisherSubId: rightRectInnerCode
					}
				} 
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 23318777
					}
				}  
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1054371'
				// 	}
				// }   
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'joemonster-org',
						adUnitElementId: rightRectInnerCode,
						placement: rightRectInnerCode,
						environment: 'desktop'
					}
				}
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				//         placementId: "122108155712562"
				//     }
				// } 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'zVYsQoXylh',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		}   
		,
		{
			code: rightSkyInnerCode,
			mediaTypes: {
				banner: {
					sizes: [[300, 600]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '91830'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 154914,
						pageId: 930694,
						formatId: 65870
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 930643,
						pageId: 930643,
						formatId: 55808
					}
				}
				,
				{
					bidder: 'sspBC'
				}  
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '396252',
						zoneId: '2212966',
						sizes: '10'
					}
				}   
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418176,
						publisherSubId: rightSkyInnerCode
					}
				} 
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 23318777
					}
				}  
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1054371'
				// 	}
				// }   
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'joemonster-org',
						adUnitElementId: rightSkyInnerCode,
						placement: rightSkyInnerCode,
						environment: 'desktop'
					}
				}
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				//         placementId: "122108155710393"
				//     }
				// } 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: '6uSEeIdm8v',
						supplyType: "site"
				    }
				}  
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		}   
		,
		{
			code: middleRectInnerCode,
			mediaTypes: {
				banner: {
					sizes: [[300, 250],[336, 280]]
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '186154'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 154914,
						pageId: 930698,
						formatId: 65871
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 167862,
						pageId: 930655,
						formatId: 54306
					}
				}
				,
				{
					bidder: 'sspBC'
				}  
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '396252',
						zoneId: '2212970',
						sizes: '16'
					}
				} 
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418180,
						publisherSubId: middleRectInnerCode
					}
				} 
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 23318777
					}
				}  
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1054371'
				// 	}
				// }   
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'joemonster-org',
						adUnitElementId: middleRectInnerCode,
						placement: middleRectInnerCode,
						environment: 'desktop'
					}
				}
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				// 		placementId: "122108155712562"
				// 	}
				// } 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'zVYsQoXylh',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		}   
		,
		{
			code: bottomRectInnerCode,
			mediaTypes: {
				banner: {
					sizes: [[300, 250],[336, 280]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '186156'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 154914,
						pageId: 930699,
						formatId: 65872
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 167862,
						pageId: 930656,
						formatId: 54307
					}
				}
				,
				{
					bidder: 'sspBC'
				}  
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '396252',
						zoneId: '2212970',
						sizes: '16'
					}
				}  
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418180,
						publisherSubId: bottomRectInnerCode
					}
				} 
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 23318777
					}
				}  
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1054371'
				// 	}
				// }   
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'joemonster-org',
						adUnitElementId: bottomRectInnerCode,
						placement: bottomRectInnerCode,
						environment: 'desktop'
					}
				}
				// ,
				// {
				// 	bidder: 'adpone',
				// 	params: {
				// 		placementId: "122108155712562"
				// 	}
				// } 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'zVYsQoXylh',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		}    
	];

	pbjs = pbjs || {};
	pbjs.que = pbjs.que || [];

	pbjs.que.push(function() {
		pbjs.addAdUnits(adUnits);
		pbjs.setConfig({
			userSync: {
				syncEnabled: true,
				filterSettings: {        
					all: {        
						bidders: "*",        
						filter: "include"
					}        
				},        
				syncsPerBidder: 5,        
				syncDelay: 3000,        
				auctionDelay: 0,        
				userIds: [
					{                
						name: "criteo",
					}
				]
			},
			schain: { 
				validation: "strict", 
				config: { 
					ver: "1.0", 
					complete: 1, 
					nodes: [
						{ 
							asi: "yieldriser.com", 
							sid: "27", 
							hp: 1 
						}
					]
				}
			},
			bidderSequence: "random",
			disableAjaxTimeout: true,
			consentManagement: {
				cmpApi: 'iab',
				timeout: 5000,
				allowAuctionWithoutConsent: true
			},
			criteo: {
				storageAllowed: true, // opt-in to allow Criteo bid adapter to access the storage
			},
			currency: {
				adServerCurrency: "PLN",
			}
		});
		pbjs.requestBids({
			bidsBackHandler: initAdserver,
			timeout: PREBID_TIMEOUT
		});
	});
}