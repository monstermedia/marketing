if(g_ads_enabled && MMG.isAdblockDisabled()){
	
	var googletag = null;
	var pbjs = null;

	const PREBID_TIMEOUT = 3000;
	const VIGNETTE_TIMEOUT = 5000;
	const AD_REFRESH_INTERVAL = 10000;

	//div containers

	const topBill = document.getElementById('top_bill');
	const topBillInnerCode = 'top_bill_inner';
	const topBillInner = document.getElementById(topBillInnerCode);

	const skyRight = document.getElementById('dmty-sky-right');
	const skyRightInnerCode = 'right_sky_inner';
	const skyRightInner = document.getElementById(skyRightInnerCode);

	const topRect = document.getElementById('top_rect');
	const topRectInnerCode = 'top_rect_inner';
	const topRectInner = document.getElementById(topRectInnerCode);

	const middleRect = document.getElementById('middle_rect');
	const middleRectInnerCode = 'middle_rect_inner';
	const middleRectInner = document.getElementById(middleRectInnerCode);

	const bottomRect = document.getElementById('bottom_rect');
	const bottomRectInnerCode = 'bottom_rect_inner';
	const bottomRectInner = document.getElementById(bottomRectInnerCode);
	
	const vignette = document.createElement('div');
	vignette.style.textAlign = 'center';
	vignette.style.color = 'grey';
	vignette.style.fontSize = '10px';
	vignette.innerHTML = 'Reklama';

	//google ad slots

	const AAScript = document.createElement('script');
	AAScript.async = true;
	AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');
	AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');
	top.document.head.appendChild(AAScript);

	const GPTScript = document.createElement('script');
	GPTScript.async = true;
	GPTScript.setAttribute('src', '//securepubads.g.doubleclick.net/tag/js/gpt.js');
	top.document.head.appendChild(GPTScript);

	window.googletag = window.googletag || { cmd: [] };

	let slot1, slot2, slot3, slot4, slot5, slot6;
	googletag.cmd.push(function() {
		//if(topBillInner){
			slot1 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3845284974/demotywatory.pl_top_bill', [[750, 200],[750, 100],[728, 90]], topBillInnerCode) //top_bill_inner
            	.addService(googletag.pubads());
		//}
		//if(skyRightInner){
			slot3 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3845284974/demotywatory.pl_right_sky', [[300, 600]], skyRightInnerCode) //right_sky_inner
            	.addService(googletag.pubads());
		//}
		//if(topRectInner){
			slot4 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3845284974/demotywatory.pl_top_rect', [[336, 280],[300, 250]], topRectInnerCode) //top_rect_inner
            	.addService(googletag.pubads());     
		//}
		//if(middleRectInner){
			slot5 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3845284974/demotywatory.pl_middle_rect', [[336, 280],[300, 250]], middleRectInnerCode) //middle_rect_inner
            .addService(googletag.pubads());
		//}
		//if(bottomRectInner){
			slot6 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3845284974/demotywatory.pl_bottom_rect', [[336, 280],[300, 250],[728, 90]], bottomRectInnerCode) //bottom_rect_inner
            	.addService(googletag.pubads());
		//}

		googletag.pubads().enableSingleRequest();
		googletag.pubads().disableInitialLoad();
		googletag.pubads().collapseEmptyDivs();
		googletag.pubads().setForceSafeFrame(false);
		googletag.pubads().setCentering(true);
		googletag.enableServices();
	});

	googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];

	//prebid currencies

	pbjs = pbjs || {};
	pbjs.que = pbjs.que || [];
		
	// fetch('https://prebid-config-open-data.s3.eu-central-1.amazonaws.com/currencies')
	// .then(resp => resp.json())
	// .then(data => {
	// 	console.log("Aktualne kursy walut: ", data);
	// 	const USD = data.usd.rate;
	// 	const EUR = data.eur.rate;

		pbjs.bidderSettings = {
			criteo: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
				,
				storageAllowed: true
			}
			,
			adform: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.9;
				}
			}
			,
			smartadserver: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			rtbhouse: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			pulsepoint: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			onedisplay: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.85;
				}
			}
			,
			oftmedia: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.85;
				}
			}
			,
			imonomy: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			rubicon: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.75;
				}
			}
			,
			ix: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			// ,
			// connectad: {
			// 	bidCpmAdjustment: function(bidCpm) {
			// 		return bidCpm;
			// 	}
			// }
			,
			amx: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			adagio: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				},
				storageAllowed: true 
			}
			,
			between: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			} 
			,
			standard: {
				adserverTargeting: [ 
					{
						key: "hb_bidder",
						val: function (bidResponse) {
							return bidResponse.bidderCode;
						}
					}
					,
					{
						key: "hb_adid",
						val: function (bidResponse) {
							return bidResponse.adId;
						}
					}
					,
					{
						key: "hb_pb",
						val: function(bidResponse) {
							const cpm = bidResponse.cpm;
							if (cpm < 10.00) {
								return (Math.floor(cpm * 100) / 100).toFixed(2);
							}
							else {
								return '10.00';
							}
						}
					}
				]
			}
		};
	// });

	//prebid initialization function

	function initAdserver() {
		if (pbjs.initAdserverSet) return;
		pbjs.initAdserverSet = true;
		googletag.cmd.push(function() {
			pbjs.que.push(function() {
				pbjs.setTargetingForGPTAsync();
				googletag.pubads().refresh();
			});
		});
	}

	//ad refresh functions

	function createObserver({interval, element, ratio}) {
		const observer = new IntersectionObserver(entries => {
			handleIntersect({
				entries: entries, 
				interval: interval,
				ratio: ratio
			})
		}, { threshold: 1 });
		observer.observe(element);
	}

	function handleIntersect({entries, interval, ratio}) {
		entries.forEach(entry => {
			interval.clear();
			if(entry.intersectionRatio > ratio.get() && !document.hidden) {
				interval.set();
			};
			ratio.set(entry.intersectionRatio);
		});
	}

	function refreshBid({elementCode, slot}) {
		console.log("refreshAdUnit " + elementCode)
		pbjs.que.push(function() {
			pbjs.requestBids({
				timeout: PREBID_TIMEOUT,
				adUnitCodes: [elementCode],
				bidsBackHandler: function() {
					pbjs.setTargetingForGPTAsync([elementCode]);
					googletag.pubads().refresh([slot]);
					googletag.cmd.push(function() {
						googletag.display(elementCode);
					});
				}
			});
		});
	}

	//adunit refresh inits and settings
	
	//top_bill_inner

	let topBillInterval;
	const topBillIntervalSet = () => {
		topBillInterval = setInterval(() => {
			refreshBid({
				elementCode: topBillInnerCode, 
				slot: slot1
			})
		}, AD_REFRESH_INTERVAL);
	}
	const topBillIntervalClear = () => clearInterval(topBillInterval)

	let topBillInnerRatio = 0;
	const setTopBillRatio = (x) => { topBillInnerRatio = x }
	const getTopBillRatio = () => { return topBillInnerRatio }

	if(topBillInner){
		window.addEventListener("load", () => {
			createObserver({
				element: topBillInner,
				interval: {
					set: topBillIntervalSet,
					clear: topBillIntervalClear
				},
				ratio: {
					set: setTopBillRatio,
					get: getTopBillRatio
				}
			});
		}, false);
	}

	//right_sky_inner

	let rightSkyInterval;
	const rightSkyIntervalSet = () => {
		rightSkyInterval = setInterval(() => {
			refreshBid({
				elementCode: skyRightInnerCode, 
				slot: slot3
			})
		}, AD_REFRESH_INTERVAL);
	}
	const rightSkyIntervalClear = () => clearInterval(rightSkyInterval)

	let rightSkyInnerRatio = 0;
	const setRightSkyRatio = (x) => { rightSkyInnerRatio = x }
	const getRightSkyRatio = () => { return rightSkyInnerRatio }

	if(skyRightInner){
		window.addEventListener("load", () => {
			createObserver({
				element: skyRightInner,
				interval: {
					set: rightSkyIntervalSet,
					clear: rightSkyIntervalClear
				},
				ratio: {
					set: setRightSkyRatio,
					get: getRightSkyRatio
				}
			});
		}, false);
	}

	//top_rect_inner

	let topRectInterval;
	const topRectIntervalSet = () => {
		topRectInterval = setInterval(() => {
			refreshBid({
				elementCode: topRectInnerCode, 
				slot: slot4
			})
		}, AD_REFRESH_INTERVAL);
	}
	const topRectIntervalClear = () => clearInterval(topRectInterval)

	let topRectInnerRatio = 0;
	const setTopRectRatio = (x) => { topRectInnerRatio = x }
	const getTopRectRatio = () => { return topRectInnerRatio }

	if(topRectInner){
		window.addEventListener("load", () => {
			createObserver({
				element: topRectInner,
				interval: {
					set: topRectIntervalSet,
					clear: topRectIntervalClear
				},
				ratio: {
					set: setTopRectRatio,
					get: getTopRectRatio
				}
			});
		}, false);
	}

	//middle_rect_inner

	let middleRectInterval;
	const middleRectIntervalSet = () => {
		middleRectInterval = setInterval(() => {
			refreshBid({
				elementCode: middleRectInnerCode, 
				slot: slot5
			})
		}, AD_REFRESH_INTERVAL);
	}
	const middleRectIntervalClear = () => clearInterval(middleRectInterval)

	let middleRectInnerRatio = 0;
	const setMiddleRectRatio = (x) => { middleRectInnerRatio = x }
	const getMiddleRectRatio = () => { return middleRectInnerRatio }

	if(middleRectInner){
		window.addEventListener("load", () => {
			createObserver({
				element: middleRectInner,
				interval: {
					set: middleRectIntervalSet,
					clear: middleRectIntervalClear
				},
				ratio: {
					set: setMiddleRectRatio,
					get: getMiddleRectRatio
				}
			});
		}, false);
	}

	//bottom_rect_inner

	let bottomRectInterval;
	const bottomRectIntervalSet = () => {
		bottomRectInterval = setInterval(() => {
			refreshBid({
				elementCode: bottomRectInnerCode, 
				slot: slot6
			})
		}, AD_REFRESH_INTERVAL);
	}
	const bottomRectIntervalClear = () => clearInterval(bottomRectInterval)

	let bottomRectInnerRatio = 0;
	const setBottomRectRatio = (x) => { bottomRectInnerRatio = x }
	const getBottomRectRatio = () => { return bottomRectInnerRatio }

	if(bottomRectInner){
		window.addEventListener("load", () => {
			createObserver({
				element: bottomRectInner,
				interval: {
					set: bottomRectIntervalSet,
					clear: bottomRectIntervalClear
				},
				ratio: {
					set: setBottomRectRatio,
					get: getBottomRectRatio
				}
			});
		}, false);
	}

	document.addEventListener("visibilitychange", () => {
		if(document.hidden){
			topBillIntervalClear()
			leftSkyIntervalClear()
			rightSkyIntervalClear()
			topRectIntervalClear()
			middleRectIntervalClear()
			bottomRectIntervalClear()
		}
	});

	const createVignettes = (g_ads_enabled) => {
		//if(topBill) topBill.prepend(vignette.cloneNode(true));
		//if(skyRight) skyRight.prepend(vignette.cloneNode(true));
		//if(topRect) topRect.prepend(vignette.cloneNode(true));
		//if(middleRect) middleRect.prepend(vignette.cloneNode(true));
		//if(bottomRect) bottomRect.prepend(vignette.cloneNode(true));
		
		document.getElementById('top_bill').prepend(vignette.cloneNode(true));
		document.getElementById('dmty-sky-right').prepend(vignette.cloneNode(true));
		document.getElementById('top_rect').prepend(vignette.cloneNode(true));
		document.getElementById('middle_rect').prepend(vignette.cloneNode(true));
		document.getElementById('bottom_rect').prepend(vignette.cloneNode(true));

		if(g_ads_enabled){
			const firstAutoAd = document.getElementsByClassName('google-auto-placed')[0];
			const secondAutoAd = document.getElementsByClassName('google-auto-placed')[1];

			if(firstAutoAd) firstAutoAd.prepend(vignette.cloneNode(true));
			if(secondAutoAd) secondAutoAd.prepend(vignette.cloneNode(true));
		}
	}

	setTimeout(() => {
		createVignettes(g_ads_enabled);
	}, VIGNETTE_TIMEOUT);

    //biders
    
    const adUnits = [
        {
            code: topBillInnerCode,
            mediaTypes: {
                banner: {
                    sizes: [[750, 200],[750, 100],[728, 90]],
                }
            }
            ,
            bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '1305628'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422, siteId: 154887, pageId: 792805, formatId: 51751,
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581, siteId: 167130, pageId: 834179, formatId: 56391,
					}
				}
				,
				{
					bidder: 'sspBC'
				}  
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '406510',
						zoneId:'2281134',
						sizes:'40',
					}
				}   
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418179,
						publisherSubId: topBillInnerCode
					}
				} 
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 23981220
					}
				}  
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1055768'
				// 	}
				// }   
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'demotywatory-pl',
						adUnitElementId: topBillInnerCode,
						placement: topBillInnerCode,
						environment: 'desktop',
					}
				}
				//,
				//{
				// 	bidder: 'adpone',
				//     params: {
				// 		placementId: "122108154227788"
				// 	}
				//} 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'GBTsKS3Ytj',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
        }
        ,        
        {
            code: skyRightInnerCode,
            mediaTypes: {
                banner: {
                    sizes: [[300, 600]],
                }
            }
            ,
            bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '323082'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422, siteId: 154887, pageId: 792817, formatId: 53517,
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581, siteId: 167130, pageId: 882945, formatId: 55370,
					}
				}
				,
				{
					bidder: 'sspBC'
				} 
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '406510',
						zoneId:'2281140',
						sizes:'10',
					}
				} 
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418176,
						publisherSubId: skyRightInnerCode
					}
				}  
				,
			
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 23981220
					}
				}  
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1055768'
				// 	}
				// }
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'demotywatory-pl',
						adUnitElementId: skyRightInnerCode,
						placement: skyRightInnerCode,
						environment: 'desktop',
					}
				} 
				// ,
				// {
				//  	bidder: 'adpone',
				//  	params: {
				// 			placementId: "122108154232401"
				// 	}
				// }  
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'dEBjiXagCg',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
        }
        ,
        {
            code: topRectInnerCode,
            mediaTypes: {
                banner: {
                    sizes: [[300, 250],[336, 280]],
                }
            }
            ,
            bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '344424'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422, siteId:  170496, pageId: 840780, formatId: 62779,
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581, siteId:  167130, pageId: 907610, formatId: 54303,
					}
				}
				,
				{
					bidder: 'sspBC'
				} 
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '406510',
						zoneId:'2281136',
						sizes:'16',
					}
				} 
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418180,
						publisherSubId: topRectInnerCode
					}
				} 
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				} 
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 23981220
					}
				}  
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1055768'
				// 	}
				// }
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'demotywatory-pl',
						adUnitElementId: topRectInnerCode,
						placement: topRectInnerCode,
						environment: 'mobile',
					}
				}
				//,
				// {
				//     bidder: 'adpone',
				//     params: {
				//         placementId: "122108154259386"
				//     }
				// } 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'LqlUbjXh7y',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
        }    
        ,
		{
			code: middleRectInnerCode,
			mediaTypes: {
				banner: {
					sizes: [[300, 250],[336, 280]],
				}
			}
			,
			bids: [
				{
					bidder: 'adform',
					params: {
						mid: '344425'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422, siteId:  154887, pageId: 792810, formatId: 52161,
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581, siteId:  167130, pageId: 907611, formatId: 54304,
					}
				}
				,
				{
					bidder: 'sspBC'
				} 
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '406510',
						zoneId:'2281136',
						sizes:'16',
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418180,
						publisherSubId: middleRectInnerCode
					}
				} 
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 23981220
					}
				}  
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1055768'
				// 	}
				// }
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'demotywatory-pl',
						adUnitElementId: middleRectInnerCode,
						placement: middleRectInnerCode,
						environment: 'mobile',
					}
				}
				,
				// {
				// 	bidder: 'adpone',
				// 	params:{
				// 		placementId: "122108154259386"
				// 	}
				// }  
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'LqlUbjXh7y',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		}      
		,
		{
			code: bottomRectInnerCode,
			mediaTypes: {
				banner: {
					sizes: [[300, 250],[336, 280],[728, 90]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '344427'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422, siteId:  154887, pageId: 792814, formatId: 52161,
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581, siteId:  167130, pageId: 1045260, formatId: 54306,
					}
				}
				,
				{
					bidder: 'sspBC'
				} 
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '406510',
						zoneId:'2281138',
						sizes:'15',
					}
				}  
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418175,
						publisherSubId: bottomRectInnerCode
					}
				}   
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 23981220
					}
				}  
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1055768'
				// 	}
				// }
				,
				{
					bidder: 'adagio',
					params:{
						organizationId: '1120',
						site: 'demotywatory-pl',
						adUnitElementId: bottomRectInnerCode,
						placement: bottomRectInnerCode,
						environment: 'desktop',
					}
				} 
				,
				// {
				// 	bidder: 'adpone',
				// 	params:{
				// 		placementId: "122108154259386"
				// 	}
				// }  
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'LqlUbjXh7y',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		}    
    ];

	pbjs = pbjs || {};
	pbjs.que = pbjs.que || [];
	
	pbjs.que.push(function() {
		pbjs.addAdUnits(adUnits);
		pbjs.setConfig({
			userSync: {
				syncEnabled: true,
				filterSettings: {        
					all: {        
						bidders: "*",        
						filter: "include"
					}        
				},        
				syncsPerBidder: 5,        
				syncDelay: 3000,        
				auctionDelay: 0,        
				userIds: [
					{                
						name: "criteo",
					}
				]
			},
			schain: { 
				validation: "strict", 
				config: { 
					ver: "1.0", 
					complete: 1, 
					nodes: [
						{ 
							asi: "yieldriser.com", 
							sid: "27", 
							hp: 1 
						}
					]
				}
			},
			bidderSequence: "random",
			disableAjaxTimeout: true,
			consentManagement: {
				cmpApi: 'iab',
				timeout: 5000,
				allowAuctionWithoutConsent: true
			},
			criteo: {
				storageAllowed: true, // opt-in to allow Criteo bid adapter to access the storage
			},
			currency: {
				adServerCurrency: "PLN",
			}
		});
		pbjs.requestBids({
			bidsBackHandler: initAdserver,
			timeout: PREBID_TIMEOUT
		});
	});
}