if(g_ads_enabled){
	
	var googletag = null;
	var pbjs = null;

	const PREBID_TIMEOUT = 1200;
	const VIGNETTE_TIMEOUT = 5000;

	//div containers

	const topBillCode = 'top_bill';
	const topBill = document.getElementById(topBillCode);

	const middleRectCode = 'middle_rect';
	const middleRect = document.getElementById(middleRectCode);

	const bottomRectCode = 'bottom_rect';
	const bottomRect = document.getElementById(bottomRectCode);
	
	const vignette = document.createElement('div');
	vignette.style.textAlign = 'center';
	vignette.style.color = 'grey';
	vignette.style.fontSize = '10px';
	vignette.innerHTML = 'Reklama';

	//google ad slots   
    
    const AAScript = document.createElement('script');
	AAScript.async = true;
    AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');      
    AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');      
    top.document.head.appendChild(AAScript);  

	const GPTScript = document.createElement('script');
	GPTScript.async = true;
	GPTScript.setAttribute('src', '//securepubads.g.doubleclick.net/tag/js/gpt.js');
	top.document.head.appendChild(GPTScript);

	window.googletag = window.googletag || { cmd: [] };

	googletag.cmd.push(function() {
		// if(topBill){
			googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/2703156774/2870248169', [[336, 280],[300, 250]], topBillCode) //top_bill
            	.addService(googletag.pubads());
		// }
		// if(middleRect){
			googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/2703156774/faktopedia.pl_300x250_mobile_middle_rect', [[336, 280],[300, 250]], middleRectCode) //middle_rect
            	.addService(googletag.pubads());
		// }
		// if(bottomRect){
			googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/2703156774/faktopedia.pl_300x250_mobile_bottom_rect', [[336, 280],[300, 250]], bottomRectCode) //bottom_rect
            	.addService(googletag.pubads());
		// }

		googletag.pubads().enableSingleRequest();
		googletag.pubads().disableInitialLoad();
		googletag.pubads().collapseEmptyDivs();
		googletag.pubads().setForceSafeFrame(false);
		googletag.pubads().setCentering(true);
		googletag.enableServices();
	});

	googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];

	//prebid currencies

	pbjs = pbjs || {};
	pbjs.que = pbjs.que || [];
		
	// fetch('https://prebid-config-open-data.s3.eu-central-1.amazonaws.com/currencies')
    // .then(resp => resp.json())
    // .then(data => {
    //     console.log("Aktualne kursy walut: ", data);
         
    //     const USD = data.usd.rate;
    //     const EUR = data.eur.rate;
    
        pbjs.bidderSettings = {
			criteo: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
				,
				storageAllowed: true 
			}
			,
			adform: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.9;
				}
			}
			,
			smartadserver: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			rtbhouse: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			pulsepoint: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			onedisplay: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.85;
				}
			}
			,
			oftmedia: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.85;
				}
			}
			,
			imonomy: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			rubicon: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.75;
				}
			}
			,
			ix: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			// ,
			// connectad: {
			// 	bidCpmAdjustment: function(bidCpm) {
			// 		return bidCpm;
			// 	}
			// }
			,
			amx: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			visx: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			sspBC: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			adagio: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				},
				storageAllowed: true 
			}
			,
			standard: {
				adserverTargeting: [ 
					{
						key: "hb_bidder",
						val: function (bidResponse) {
							return bidResponse.bidderCode;
						}
					}
					,
					{
						key: "hb_adid",
						val: function (bidResponse) {
							return bidResponse.adId;
						}
					}
					,
					{
						key: "hb_pb",
						val: function(bidResponse) {
							const cpm = bidResponse.cpm;
							if (cpm < 10.00) {
								return (Math.floor(cpm * 100) / 100).toFixed(2);
							}
							else {
								return '10.00';
							}
						}
					}
				]
			}
        }
    // });

	//prebid initialization function

	function initAdserver() {
		if (pbjs.initAdserverSet) return;
		pbjs.initAdserverSet = true;
		googletag.cmd.push(function() {
			pbjs.que.push(function() {
				pbjs.setTargetingForGPTAsync();
				googletag.pubads().refresh();
			});
		});
	}

	const createVignettes = (g_ads_enabled) => {
		// if(topBill) topBill.prepend(vignette.cloneNode(true));
		// if(middleRect) middleRect.prepend(vignette.cloneNode(true));
		// if(bottomRect) bottomRect.prepend(vignette.cloneNode(true));

		if(g_ads_enabled){
			const firstAutoAd = document.getElementsByClassName('google-auto-placed')[0];
			const secondAutoAd = document.getElementsByClassName('google-auto-placed')[1];
			const thirdAutoAd = document.getElementsByClassName('google-auto-placed')[2];
			const fourthAutoAd = document.getElementsByClassName('google-auto-placed')[3];
			const fifthAutoAd = document.getElementsByClassName('google-auto-placed')[4];

			if(firstAutoAd) firstAutoAd.prepend(vignette.cloneNode(true));
			if(secondAutoAd) secondAutoAd.prepend(vignette.cloneNode(true));
			if(thirdAutoAd) thirdAutoAd.prepend(vignette.cloneNode(true));
			if(fourthAutoAd) fourthAutoAd.prepend(vignette.cloneNode(true));
			if(fifthAutoAd) fifthAutoAd.prepend(vignette.cloneNode(true));
		}
	}

	setTimeout(() => {
		createVignettes(g_ads_enabled);
	}, VIGNETTE_TIMEOUT);
	   
    //biders
    
	const adUnits = [   
		{
			code: topBillCode,
			mediaTypes: {
				banner: {
					sizes: [[300, 250],[336, 280]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '451467'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422, siteId: 150872, pageId: 780349, formatId: 52161,
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581, siteId: 461666, pageId: 1451617, formatId: 55373,
					}
				} 
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '386578',
						zoneId:'2153596',
						sizes:'15',
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418175,
						publisherSubId: topBillCode
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				} 
				,
				{
					bidder: 'sspBC'
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 22783799
					}
				} 
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1053218'
				// 	}
				// }
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'faktopedia-pl',
						adUnitElementId: topBillCode,
						placement: topBillCode,
						environment: 'mobile',
					}
				}
				,
				{
					bidder: 'adpone',
					params: {
						placementId: "122108154911234"
					}
				} 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'yCkjzB1bUq',
						supplyType: "site"
				    }
				}              
				,
				{
					bidder: 'visx',
					params: {
						uid: '915022'
					}
				}
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		}  
		,
		{
			code: middleRectCode,
			mediaTypes: {
				banner: {
					sizes: [[300, 250],[336, 280]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '1136704'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422, siteId:  150872, pageId: 780350, formatId: 52161,
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581, siteId:  461666, pageId: 1472298, formatId: 54303,
					}
				}
				,
				{
					bidder: 'sspBC'
				} 
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '386578',
						zoneId:'2153596',
						sizes:'15',
					}
				}  
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418175,
						publisherSubId: middleRectCode
					}
				} 
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				} 
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 22783799
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1053218'
				// 	}
				// }
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'faktopedia-pl',
						adUnitElementId: middleRectCode,
						placement: middleRectCode,
						environment: 'mobile',
					}
				}
				,
				{
					bidder: 'adpone',
					params: {
						placementId: "122108154911234"
					}
				}   
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'yCkjzB1bUq',
						supplyType: "site"
				    }
				}                
				,
				{
					bidder: 'visx',
					params: {
						uid: '915020'
					}
				}
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
			]
		} 
		,
		{
			code: bottomRectCode,
			mediaTypes: {
				banner: {
					sizes: [
						[300, 250],
						[336, 280]
					]
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '1136705'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422, siteId:  150872, pageId: 780351, formatId: 52161,
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581, siteId:  461666, pageId: 1472299, formatId: 54304,
					}
				}
				,
				{
					bidder: 'sspBC'
				} 
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '386578',
						zoneId:'2153594',
						sizes:'15',
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						zoneId: 1418180,
						publisherSubId: bottomRectCode
					}
				} 
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}  
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 22783799
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1053218'
				// 	}
				// }
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'faktopedia-pl',
						adUnitElementId: bottomRectCode,
						placement: bottomRectCode,
						environment: 'mobile',
					}
				} 
				,
				{
					bidder: 'adpone',
					params: {
						placementId: "122108154911234"
					}
				} 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'yCkjzB1bUq',
						supplyType: "site"
				    }
				}   
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// }                    
			]
		}
		,
		{
			code: 'faktopedia.pl_pixel',
			mediaTypes: {
				banner: {
					sizes: [
						[1, 1]
					]
				}
			}
			,
			bids: [ 
				{
					bidder: 'visx',
					params: {
						uid: '922870'
					}
				}
			]
		}
	];

	pbjs = pbjs || {};
	pbjs.que = pbjs.que || [];

	pbjs.que.push(function() {
		pbjs.addAdUnits(adUnits);
		pbjs.setConfig({
			userSync: {
				syncEnabled: true,
				filterSettings: {        
					all: {        
						bidders: "*",        
						filter: "include"
					}        
				},        
				syncsPerBidder: 5,        
				syncDelay: 3000,        
				auctionDelay: 0,        
				userIds: [
					{                
						name: "criteo",
					}
				]
			},
			schain: { 
				validation: "strict", 
				config: { 
					ver: "1.0", 
					complete: 1, 
					nodes: [
						{ 
							asi: "yieldriser.com", 
							sid: "27", 
							hp: 1 
						}
					]
				}
			},
			bidderSequence: "random",
			disableAjaxTimeout: true,
			consentManagement: {
				cmpApi: 'iab',
				timeout: 5000,
				allowAuctionWithoutConsent: true
			},
			criteo: {
				storageAllowed: true, // opt-in to allow Criteo bid adapter to access the storage
			},
			currency: {
				adServerCurrency: "PLN",
			 }
		});
		pbjs.requestBids({
			bidsBackHandler: initAdserver,
			timeout: PREBID_TIMEOUT
		});
	});
}