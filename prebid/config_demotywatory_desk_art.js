if(MMG.isAdblockDisabled()){
	
	var googletag = null;
	var pbjs = null;

	const PREBID_TIMEOUT = 3000;
	const VIGNETTE_TIMEOUT = 5000;
	const AD_REFRESH_INTERVAL = 10000;

	//div containers

	const topBill = document.getElementById('top_bill');
	const topBillInnerCode = 'top_bill_inner';
	const topBillInner = document.getElementById(topBillInnerCode);

	const skyRight = document.getElementById('dmty-sky-right');
	const skyRightInnerCode = 'right_sky_inner';
	const skyRightInner = document.getElementById(skyRightInnerCode);

	const bottomRect = document.getElementById('bottom_rect');
	const bottomRectInnerCode = 'bottom_rect_inner';
	const bottomRectInner = document.getElementById(bottomRectInnerCode);
	
	const vignette = document.createElement('div');
	vignette.style.textAlign = 'center';
	vignette.style.color = 'grey';
	vignette.style.fontSize = '10px';
	vignette.innerHTML = 'Reklama';

	//google ad slots

	const GPTScript = document.createElement('script');
	GPTScript.async = true;
	GPTScript.setAttribute('src', '//securepubads.g.doubleclick.net/tag/js/gpt.js');
	top.document.head.appendChild(GPTScript);

	window.googletag = window.googletag || { cmd: [] };

	let slot1, slot2, slot3, slot4;
	googletag.cmd.push(function() {
		//if(topBillInner){
			slot1 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3845284974/demotywatory.pl_top_bill', [[750, 200],[750, 100],[728, 90]], topBillInnerCode) //top_bill_inner
				.addService(googletag.pubads());
		//}
		//if(skyRightInner){
			slot3 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3845284974/demotywatory.pl_right_sky', [[300, 600]], skyRightInnerCode) //right_sky_inner
				.addService(googletag.pubads());
		//}
		//if(bottomRectInner){
			slot4 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3845284974/demotywatory.pl_bottom_rect', [[300, 250],[336, 280],[728, 90]], bottomRectInnerCode) //bottom_rect_inner
				.addService(googletag.pubads());
		//}

		googletag.pubads().enableSingleRequest();
		googletag.pubads().disableInitialLoad();
		googletag.pubads().collapseEmptyDivs();
		googletag.pubads().setForceSafeFrame(false);
		googletag.pubads().setCentering(true);
		googletag.enableServices();
	});

	googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];

	//prebid currencies

	pbjs = pbjs || {};
	pbjs.que = pbjs.que || [];
		
	// fetch('https://prebid-config-open-data.s3.eu-central-1.amazonaws.com/currencies')
	// .then(resp => resp.json())
	// .then(data => {
	// 	console.log("Aktualne kursy walut: ", data);
	// 	const USD = data.usd.rate;
	// 	const EUR = data.eur.rate;

		pbjs.bidderSettings = {
			criteo: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
				,
				storageAllowed: true
			}
			,
			adform: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.9;
				}
			}
			,
			smartadserver: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			rtbhouse: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			pulsepoint: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			onedisplay: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.85;
				}
			}
			,
			oftmedia: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.85;
				}
			}
			,
			imonomy: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			rubicon: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.75;
				}
			}
			,
			ix: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			// ,
			// connectad: {
			// 	bidCpmAdjustment: function(bidCpm) {
			// 		return bidCpm;
			// 	}
			// }
			,
			amx: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			adagio: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				},
				storageAllowed: true 
			}
			,
			standard: {
				adserverTargeting: [ 
					{
						key: "hb_bidder",
						val: function (bidResponse) {
							return bidResponse.bidderCode;
						}
					}
					,
					{
						key: "hb_adid",
						val: function (bidResponse) {
							return bidResponse.adId;
						}
					}
					,
					{
						key: "hb_pb",
						val: function(bidResponse) {
							const cpm = bidResponse.cpm;
							if (cpm < 10.00) {
								return (Math.floor(cpm * 100) / 100).toFixed(2);
							}
							else {
								return '10.00';
							}
						}
					}
				]
			}
		};
	// });

	//prebid initialization function

	function initAdserver() {
		if (pbjs.initAdserverSet) return;
		pbjs.initAdserverSet = true;
		googletag.cmd.push(function() {
			pbjs.que.push(function() {
				pbjs.setTargetingForGPTAsync();
				googletag.pubads().refresh();
			});
		});
	}

	//ad refresh functions

	function createObserver({interval, element, ratio}) {
		const observer = new IntersectionObserver(entries => {
			handleIntersect({
				entries: entries, 
				interval: interval,
				ratio: ratio
			})
		}, { threshold: 1 });
		observer.observe(element);
	}

	function handleIntersect({entries, interval, ratio}) {
		entries.forEach(entry => {
			interval.clear();
			if(entry.intersectionRatio > ratio.get() && !document.hidden) {
				interval.set();
			};
			ratio.set(entry.intersectionRatio);
		});
	}

	function refreshBid({elementCode, slot}) {
		console.log("refreshAdUnit " + elementCode)
		pbjs.que.push(function() {
			pbjs.requestBids({
				timeout: PREBID_TIMEOUT,
				adUnitCodes: [elementCode],
				bidsBackHandler: function() {
					pbjs.setTargetingForGPTAsync([elementCode]);
					googletag.pubads().refresh([slot]);
					googletag.cmd.push(function() {
						googletag.display(elementCode);
					});
				}
			});
		});
	}

	//adunit refresh inits and settings
	
	//top_bill_inner

	let topBillInterval;
	const topBillIntervalSet = () => {
		topBillInterval = setInterval(() => {
			refreshBid({
				elementCode: topBillInnerCode, 
				slot: slot1
			})
		}, AD_REFRESH_INTERVAL);
	}
	const topBillIntervalClear = () => clearInterval(topBillInterval)

	let topBillInnerRatio = 0;
	const setTopBillRatio = (x) => { topBillInnerRatio = x }
	const getTopBillRatio = () => { return topBillInnerRatio }

	if(topBillInner){
		window.addEventListener("load", () => {
			createObserver({
				element: topBillInner,
				interval: {
					set: topBillIntervalSet,
					clear: topBillIntervalClear
				},
				ratio: {
					set: setTopBillRatio,
					get: getTopBillRatio
				}
			});
		}, false);
	}

	//right_sky_inner

	let rightSkyInterval;
	const rightSkyIntervalSet = () => {
		rightSkyInterval = setInterval(() => {
			refreshBid({
				elementCode: skyRightInnerCode, 
				slot: slot3
			})
		}, AD_REFRESH_INTERVAL);
	}
	const rightSkyIntervalClear = () => clearInterval(rightSkyInterval)

	let rightSkyInnerRatio = 0;
	const setRightSkyRatio = (x) => { rightSkyInnerRatio = x }
	const getRightSkyRatio = () => { return rightSkyInnerRatio }

	if(skyRightInner){
		window.addEventListener("load", () => {
			createObserver({
				element: skyRightInner,
				interval: {
					set: rightSkyIntervalSet,
					clear: rightSkyIntervalClear
				},
				ratio: {
					set: setRightSkyRatio,
					get: getRightSkyRatio
				}
			});
		}, false);
	}

	//bottom_rect_inner

	let bottomRectInterval;
	const bottomRectIntervalSet = () => {
		bottomRectInterval = setInterval(() => {
			refreshBid({
				elementCode: bottomRectInnerCode, 
				slot: slot4
			})
		}, AD_REFRESH_INTERVAL);
	}
	const bottomRectIntervalClear = () => clearInterval(bottomRectInterval)

	let bottomRectInnerRatio = 0;
	const setBottomRectRatio = (x) => { bottomRectInnerRatio = x }
	const getBottomRectRatio = () => { return bottomRectInnerRatio }

	if(bottomRectInner){
		window.addEventListener("load", () => {
			createObserver({
				element: bottomRectInner,
				interval: {
					set: bottomRectIntervalSet,
					clear: bottomRectIntervalClear
				},
				ratio: {
					set: setBottomRectRatio,
					get: getBottomRectRatio
				}
			});
		}, false);
	}

	document.addEventListener("visibilitychange", () => {
		if(document.hidden){
			topBillIntervalClear()
			leftSkyIntervalClear()
			rightSkyIntervalClear()
			bottomRectIntervalClear()
		}
	});

	const createVignettes = (g_ads_enabled) => {
		//if(topBill) topBill.prepend(vignette.cloneNode(true));
		//if(skyRight) skyRight.prepend(vignette.cloneNode(true));
		//if(bottomRect) bottomRect.prepend(vignette.cloneNode(true));
		
		document.getElementById('top_bill').prepend(vignette.cloneNode(true));
		document.getElementById('dmty-sky-right').prepend(vignette.cloneNode(true));
		document.getElementById('bottom_rect').prepend(vignette.cloneNode(true));

		if(g_ads_enabled){
			const firstAutoAd = document.getElementsByClassName('google-auto-placed')[0];
			const secondAutoAd = document.getElementsByClassName('google-auto-placed')[1];
			const thirdAutoAd = document.getElementsByClassName('google-auto-placed')[2];
			const fourthAutoAd = document.getElementsByClassName('google-auto-placed')[3];
			const fifthAutoAd = document.getElementsByClassName('google-auto-placed')[4];
			const sixthAutoAd = document.getElementsByClassName('google-auto-placed')[5];

			if(firstAutoAd) firstAutoAd.prepend(vignette.cloneNode(true));
			if(secondAutoAd) secondAutoAd.prepend(vignette.cloneNode(true));
			if(thirdAutoAd) thirdAutoAd.prepend(vignette.cloneNode(true));
			if(fourthAutoAd) fourthAutoAd.prepend(vignette.cloneNode(true));
			if(fifthAutoAd) fifthAutoAd.prepend(vignette.cloneNode(true));
			if(sixthAutoAd) sixthAutoAd.prepend(vignette.cloneNode(true));
		}
	}

	//separate configs for autoads enabled and disabled

	if(g_ads_enabled){

		const AAScript = document.createElement('script');
		AAScript.async = true;
		AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');
		AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');
		top.document.head.appendChild(AAScript);

		setTimeout(() => {
			createVignettes(g_ads_enabled);
		}, VIGNETTE_TIMEOUT);

		//biders

		const adUnits = [
			{
				code: topBillInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[750, 200],[750, 100],[728, 90]],
					}
				}
				,
				bids: [
					{
						bidder: 'adform',
						params: {
							mid: '1305628'
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId: 154887, pageId: 792805, formatId: 51751,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId: 167130, pageId: 834179, formatId: 56391,
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281134',
							sizes:'40',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1418179,
							publisherSubId: topBillInnerCode
						}
					}
					,
					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params: {
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: topBillInnerCode,
							placement: topBillInnerCode,
							environment: 'desktop',
						}
					}
					//,
					//{
					// 	bidder: 'adpone',
					// 	params:{
					// 		placementId: "122108154227788"
					// 	}
					//}
					,
					{
						bidder: 'richaudience',
						params: {
							pid: 'GBTsKS3Ytj',
							supplyType: "site"
						}
					}
					// ,
					// {
					// 	bidder: 'smilewanted',
					// 	params: {
					// 		zoneId: 'yieldriser.pl_hb_display',
					// 		bidfloor: 0.00
					// 	}
					// } 
				]
			}
			,
			{
				code: skyRightInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[300, 600]],
					}
				}
				,
				bids: [
					{
						bidder: 'adform',
						params: {
							mid: '323082'
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId: 154887, pageId: 792817, formatId: 53517,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId: 167130, pageId: 882945, formatId: 55370,
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281140',
							sizes:'10',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1418176,
							publisherSubId: skyRightInnerCode
						}
					}
					,
					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params: {
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: skyRightInnerCode,
							placement: skyRightInnerCode,
							environment: 'desktop',
						}
					}
					//,
					//{
					// 	bidder: 'adpone',
					// 	params:{
					// 		placementId: "122108154232401"
					// 	}
					//} 
					,
					{
						bidder: 'richaudience',
						params: {
							pid: 'dEBjiXagCg',
							supplyType: "site"
						}
					}
					// ,
					// {
					// 	bidder: 'smilewanted',
					// 	params: {
					// 		zoneId: 'yieldriser.pl_hb_display',
					// 		bidfloor: 0.00
					// 	}
					// }  
				]
			}
			,
			{
				code: bottomRectInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[300, 250],[336, 280],[728, 90]],
					}
				}
				,
				bids: [ 
					{
						bidder: 'adform',
						params: {
							mid: '344427'
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId:  154887, pageId: 792814, formatId: 52161,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId:  167130, pageId: 1045260, formatId: 54306,
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281138',
							sizes:'15',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1418175,
							publisherSubId: bottomRectInnerCode
						}
					}
					,
					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params: {
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: bottomRectInnerCode,
							placement: bottomRectInnerCode,
							environment: 'desktop',
						}
					}
					//,
					//{
					// 	bidder: 'adpone',
					// 	params:{
					// 		placementId: "122108154259386"
					// 	}
					//} 
					,
					{
						bidder: 'richaudience',
						params: {
							pid: 'LqlUbjXh7y',
							supplyType: "site"
						}
					} 
					// ,
					// {
					// 	bidder: 'smilewanted',
					// 	params: {
					// 		zoneId: 'yieldriser.pl_hb_display',
					// 		bidfloor: 0.00
					// 	}
					// } 
				]
			}
		];

		pbjs = pbjs || {};
		pbjs.que = pbjs.que || [];

		pbjs.que.push(function() {
			pbjs.addAdUnits(adUnits);
			pbjs.setConfig({
				userSync: {
					syncEnabled: true,
					filterSettings: {        
						all: {        
							bidders: "*",        
							filter: "include"
						}        
					},        
					syncsPerBidder: 5,        
					syncDelay: 3000,        
					auctionDelay: 0,        
					userIds: [
						{                
							name: "criteo",
						}
					]
				},
				schain: { 
					validation: "strict", 
					config: { 
						ver: "1.0", 
						complete: 1, 
						nodes: [
							{ 
								asi: "yieldriser.com", 
								sid: "27", 
								hp: 1 
							}
						]
					}
				},
				bidderSequence: "random",
				disableAjaxTimeout: true,
				consentManagement: {
					cmpApi: 'iab',
					timeout: 5000,
					allowAuctionWithoutConsent: true
				},
				criteo: {
					storageAllowed: true, // opt-in to allow Criteo bid adapter to access the storage
				},
				currency: {
					adServerCurrency: "PLN",
				 }
			});
			pbjs.requestBids({
				bidsBackHandler: initAdserver,
				timeout: PREBID_TIMEOUT
			});
		});

	} else {

		setTimeout(() => {
			createVignettes(g_ads_enabled);
		}, VIGNETTE_TIMEOUT);

		//biders
		
		const adUnits = [
			{
				code: topBillInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[750, 200],[750,100],[728,90]],
					}
				}
				,
				bids: [
					{
						bidder: 'adform',
						params: {
							mid: '1428055'
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId: 154887, pageId: 792805, formatId: 51751,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId: 167130, pageId: 834179, formatId: 56391,
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281134',
							sizes:'40',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1418179,
							publisherSubId: topBillInnerCode
						}
					}
					,

					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params:{
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: topBillInnerCode,
							placement: topBillInnerCode,
							environment: 'desktop',
						}
					}
					// ,
					// {
					// 	bidder: 'adpone',
					// 	params:{
					// 			placementId: "122108154227788"
					// 			}
					// }					
				]
			}
			,
			{
				code: skyLeftInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[160, 600]],
					}
				}
				,
				bids: [
					{
						bidder: 'adform',
						params: {
							mid: '1428058'
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId: 154887, pageId: 792816, formatId: 53517,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId: 167130, pageId: 1528318, formatId: 60786,
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281142',
							sizes:'9',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1421652,
							publisherSubId: skyLeftInnerCode
						}
					}
					,

					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params:{
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: skyLeftInnerCode,
							placement: skyLeftInnerCode,
							environment: 'desktop',
						}
					}
					// ,
					// {
					// 	bidder: 'adpone',
					// 	params:{
					// 			placementId: "122108154230312"
					// 			}
					// } 					
				]
			}
			,
			{
				code: skyRightInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[300, 600]],
					}
				}
				,
				bids: [ 
					{
						bidder: 'adform',
						params: {
							mid: '1428060'
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId: 154887, pageId: 792817, formatId: 53517,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId: 167130, pageId: 882945, formatId: 55370,
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281140',
							sizes:'10',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1418176,
							publisherSubId: skyRightInnerCode
						}
					}
					,
					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params: {
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: skyRightInnerCode,
							placement: skyRightInnerCode,
							environment: 'desktop',
						}
					}
					// ,
					// {
					// 	bidder: 'adpone',
					// 	params:{
					// 			placementId: "122108154232401"
					// 			}
					// } 					
				]
			}
			,
			{
				code: bottomRectInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[300, 250],[336, 280],[728, 90]],
					}
				}
				,
				bids: [ 
					{
						bidder: 'adform',
						params: {
							mid: '1428062'
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId:  154887, pageId: 792814, formatId: 52161,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId:  167130, pageId: 1045260, formatId: 54306,
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281138',
							sizes:'15',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1418175,
							publisherSubId: bottomRectInnerCode
						}
					}
					,

					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params:{
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: bottomRectInnerCode,
							placement: bottomRectInnerCode,
							environment: 'desktop',
						}
					}
					// ,
					// {
					// 	bidder: 'adpone',
					// 	params:{
					// 			placementId: "122108154259386"
					// 			}
					// } 					
				]
			}
		];

		pbjs = pbjs || {};
		pbjs.que = pbjs.que || [];

		pbjs.que.push(function() {
			pbjs.addAdUnits(adUnits);
			pbjs.setConfig({
				userSync: {
					userIds: [{
						name: "criteo",
					}]
				},
				schain: { 
					validation: "strict", 
					config: { 
						ver: "1.0", 
						complete: 1, 
						nodes: [ 
							{ 
								asi: "yieldriser.com", 
								sid: "27", 
								hp: 1
							}
						]
					}
				},
				bidderSequence: "random",
				disableAjaxTimeout: true,
				consentManagement: {
					cmpApi: 'iab',
					timeout: 5000,
					allowAuctionWithoutConsent: true
				},
				criteo: {
					storageAllowed: true, // opt-in to allow Criteo bid adapter to access the storage
				},
				currency: {
					adServerCurrency: "PLN",
				}
			});
			pbjs.requestBids({
				bidsBackHandler: initAdserver,
				timeout: PREBID_TIMEOUT
			});
		});
	}
}