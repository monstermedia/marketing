if(g_ads_enabled){
	
	var googletag = null;
	var pbjs = null;

	const PREBID_TIMEOUT = 1500;
	const VIGNETTE_TIMEOUT = 5000;
	const AD_REFRESH_INTERVAL = 10000;

	//div containers

	const halfStickyCode = 'half_sticky';
	const halfSticky = document.getElementById(halfStickyCode);

	const billboardOneCode = 'billboard';
	const billboardOne = document.getElementById(billboardOneCode);

	const billboardTwoCode = 'billboard_2';
	const billboardTwo = document.getElementById(billboardTwoCode);

	const halfOneCode = 'half_1';
	const halfOne = document.getElementById(halfOneCode);
	
	const halfTwoCode = 'half_2';
	const halfTwo = document.getElementById(halfTwoCode);
	
	const halfThreeCode = 'half_3';
	const halfThree = document.getElementById(halfThreeCode);
	
	const halfFourCode = 'half_4';
	const halfFour = document.getElementById(halfFourCode);

	const recOneCode = 'rec_1';
	const recOne = document.getElementById(recOneCode);

	const recTwoCode = 'rec_2';
	const recTwo = document.getElementById(recTwoCode);

	const recThreeCode = 'rec_3';
	const recThree = document.getElementById(recThreeCode);

	const recFourCode = 'rec_4';
	const recFour = document.getElementById(recFourCode);
	
	const vignette = document.createElement('div');
	vignette.style.textAlign = 'center';
	vignette.style.color = 'grey';
	vignette.style.fontSize = '10px';
	vignette.innerHTML = 'Reklama';

	//google ad slots   
    
    const AAScript = document.createElement('script');
	AAScript.async = true;
    AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');      
    AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');      
    top.document.head.appendChild(AAScript);  

	const GPTScript = document.createElement('script');
	GPTScript.async = true;
	GPTScript.setAttribute('src', '//securepubads.g.doubleclick.net/tag/js/gpt.js');
	top.document.head.appendChild(GPTScript);

	window.googletag = window.googletag || { cmd: [] };

	let slot1, slot2, slot3, slot4, slot5, slot6, slot7, slot8, slot9, slot10, slot11;
	googletag.cmd.push(function() {
		// if(halfSticky){
			slot1 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/4417030854', [[300, 600]], halfStickyCode) //half sticky
            	.addService(googletag.pubads());
		// }
		// if(billboardOne){
			slot2 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/5018380974', [[750, 300], [750, 200], [750, 100], [728, 90]], billboardOneCode) //billboard
            	.addService(googletag.pubads());
		// }
		// if(billboardTwo){
			slot3 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_billboard_2', [[750, 300], [750, 200], [750, 100], [728, 90]], billboardTwoCode) //billboard 2
            	.addService(googletag.pubads());
		// }
		// if(halfOne){
			slot4 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_300x600_1', [[300, 600]], halfOneCode) //half 1
            	.addService(googletag.pubads());
		// }
		// if(halfTwo){
			slot5 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_300x600_2', [[300, 600]], halfTwoCode) //half 2
            	.addService(googletag.pubads());
		// }
		// if(halfThree){
			slot6 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_300x600_3', [[300, 600]], halfThreeCode) //half 3
            	.addService(googletag.pubads());
		// }
		// if(halfFour){
			slot7 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_300x600_4', [[300, 600]], halfFourCode) //half 4
            	.addService(googletag.pubads());
		// }
		// if(recOne){
			slot8 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/9569272854', [[336, 280], [300, 250]], recOneCode) //rec_1
            	.addService(googletag.pubads());
		// }
		// if(recTwo){
			slot9 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/6033491454', [[336, 280], [300, 250]], recTwoCode) //rec_2
            	.addService(googletag.pubads());
		// }
		// if(recThree){
			slot10 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/8847154494', [[336, 280], [300, 250]], recThreeCode) //rec_3
            	.addService(googletag.pubads());
		// }
		// if(recFour){
			slot11 = googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3541673094/mistrzowie.org_336x280_4', [[336, 280], [300, 250]], recFourCode) //rec_4
            	.addService(googletag.pubads());
		// }

		googletag.pubads().enableSingleRequest();
		googletag.pubads().disableInitialLoad();
		googletag.pubads().collapseEmptyDivs();
		googletag.pubads().setForceSafeFrame(false);
		googletag.pubads().setCentering(true);
		googletag.enableServices();
	});

	googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];

	//prebid currencies

	pbjs = pbjs || {};
	pbjs.que = pbjs.que || [];
		
	// fetch('https://prebid-config-open-data.s3.eu-central-1.amazonaws.com/currencies')
	// .then(resp => resp.json())
	// .then(data => {
	// 	console.log("Aktualne kursy walut: ", data);
		
	// 	const USD = data.usd.rate;
	// 	const EUR = data.eur.rate;

		pbjs.bidderSettings = {
			criteo: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
				,
				storageAllowed: true
			}
			,
			adform: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.9;
				}
			}
			,
			smartadserver: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			rtbhouse: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			pulsepoint: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			onedisplay: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.85;
				}
			}
			,
			oftmedia: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.85;
				}
			}
			,
			imonomy: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			rubicon: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.75;
				}
			}
			,
			ix: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			// ,
			// connectad: {
			// 	bidCpmAdjustment: function(bidCpm) {
			// 		return bidCpm;
			// 	}
			// }
			,
			amx: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			sspBC: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			adagio: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				},
				storageAllowed: true 
			}
			// ,
			// between: {
			// 	bidCpmAdjustment: function(bidCpm) {
			// 		return bidCpm;
			// 	}
			// } 
			,
			standard: {
				adserverTargeting: [ 
					{
						key: "hb_bidder",
						val: function (bidResponse) {
							return bidResponse.bidderCode;
						}
					}
					,
					{
						key: "hb_adid",
						val: function (bidResponse) {
							return bidResponse.adId;
						}
					}
					,
					{
						key: "hb_pb",
						val: function(bidResponse) {
							const cpm = bidResponse.cpm;
							if (cpm < 10.00) {
								return (Math.floor(cpm * 100) / 100).toFixed(2);
							}
							else {
								return '10.00';
							}
						}
					}
				]
			}
		};
	// });

	//prebid initialization function

	function initAdserver() {
		if (pbjs.initAdserverSet) return;
		pbjs.initAdserverSet = true;
		googletag.cmd.push(function() {
			pbjs.que.push(function() {
				pbjs.setTargetingForGPTAsync();
				googletag.pubads().refresh();
			});
		});
	}

	//ad refresh functions

	function createObserver({interval, element, ratio}) {
		const observer = new IntersectionObserver(entries => {
			handleIntersect({
				entries: entries, 
				interval: interval,
				ratio: ratio
			})
		}, { threshold: 1 });
		observer.observe(element);
	}

	function handleIntersect({entries, interval, ratio}) {
		entries.forEach(entry => {
			interval.clear();
			if(entry.intersectionRatio > ratio.get() && !document.hidden) {
				interval.set();
			};
			ratio.set(entry.intersectionRatio);
		});
	}

	function refreshBid({elementCode, slot}) {
		console.log("refreshAdUnit " + elementCode)
		pbjs.que.push(function() {
			pbjs.requestBids({
				timeout: PREBID_TIMEOUT,
				adUnitCodes: [elementCode],
				bidsBackHandler: function() {
					pbjs.setTargetingForGPTAsync([elementCode]);
					googletag.pubads().refresh([slot]);
					googletag.cmd.push(function() {
						googletag.display(elementCode);
					});
				}
			});
		});
	}

	//adunit refresh inits and settings

	//half_sticky

	let halfStickyInterval;
	const halfStickyIntervalSet = () => {
		halfStickyInterval = setInterval(() => {
			refreshBid({
				elementCode: halfStickyCode, 
				slot: slot1
			})
		}, AD_REFRESH_INTERVAL);
	}
	const halfStickyIntervalClear = () => clearInterval(halfStickyInterval)

	let halfStickyRatio = 0;
	const setHalfStickyRatio = (x) => { halfStickyRatio = x }
	const getHalfStickyRatio = () => { return halfStickyRatio }

	if(halfSticky){
		window.addEventListener("load", () => {
			createObserver({
				element: halfSticky,
				interval: {
					set: halfStickyIntervalSet,
					clear: halfStickyIntervalClear
				},
				ratio: {
					set: setHalfStickyRatio,
					get: getHalfStickyRatio
				}
			});
		}, false);
	}

	//billboard

	let billboardOneInterval;
	const billboardOneIntervalSet = () => {
		billboardOneInterval = setInterval(() => {
			refreshBid({
				elementCode: billboardOneCode, 
				slot: slot2
			})
		}, AD_REFRESH_INTERVAL);
	}
	const billboardOneIntervalClear = () => clearInterval(billboardOneInterval)

	let billboardOneRatio = 0;
	const setBillboardOneRatio = (x) => { billboardOneRatio = x }
	const getBillboardOneRatio = () => { return billboardOneRatio }

	if(billboardOne){
		window.addEventListener("load", () => {
			createObserver({
				element: billboardOne,
				interval: {
					set: billboardOneIntervalSet,
					clear: billboardOneIntervalClear
				},
				ratio: {
					set: setBillboardOneRatio,
					get: getBillboardOneRatio
				}
			});
		}, false);
	}

	//billboard_2

	let billboardTwoInterval;
	const billboardTwoIntervalSet = () => {
		billboardTwoInterval = setInterval(() => {
			refreshBid({
				elementCode: billboardTwoCode, 
				slot: slot3
			})
		}, AD_REFRESH_INTERVAL);
	}
	const billboardTwoIntervalClear = () => clearInterval(billboardTwoInterval)

	let billboardTwoRatio = 0;
	const setBillboardTwoRatio = (x) => { billboardTwoRatio = x }
	const getBillboardTwoRatio = () => { return billboardTwoRatio }

	if(billboardTwo){
		window.addEventListener("load", () => {
			createObserver({
				element: billboardTwo,
				interval: {
					set: billboardTwoIntervalSet,
					clear: billboardTwoIntervalClear
				},
				ratio: {
					set: setBillboardTwoRatio,
					get: getBillboardTwoRatio
				}
			});
		}, false);
	}
	
	//half_1

	let halfOneInterval;
	const halfOneIntervalSet = () => {
		halfOneInterval = setInterval(() => {
			refreshBid({
				elementCode: halfOneCode, 
				slot: slot4
			})
		}, AD_REFRESH_INTERVAL);
	}
	const halfOneIntervalClear = () => clearInterval(halfOneInterval)

	let halfOneRatio = 0;
	const setHalfOneRatio = (x) => { halfOneRatio = x }
	const getHalfOneRatio = () => { return halfOneRatio }

	if(halfOne){
		window.addEventListener("load", () => {
			createObserver({
				element: halfOne,
				interval: {
					set: halfOneIntervalSet,
					clear: halfOneIntervalClear
				},
				ratio: {
					set: setHalfOneRatio,
					get: getHalfOneRatio
				}
			});
		}, false);
	}

	//half_2

	let halfTwoInterval;
	const halfTwoIntervalSet = () => {
		halfTwoInterval = setInterval(() => {
			refreshBid({
				elementCode: halfTwoCode, 
				slot: slot5
			})
		}, AD_REFRESH_INTERVAL);
	}
	const halfTwoIntervalClear = () => clearInterval(halfTwoInterval)

	let halfTwoRatio = 0;
	const setHalfTwoRatio = (x) => { halfTwoRatio = x }
	const getHalfTwoRatio = () => { return halfTwoRatio }

	if(halfTwo){
		window.addEventListener("load", () => {
			createObserver({
				element: halfTwo,
				interval: {
					set: halfTwoIntervalSet,
					clear: halfTwoIntervalClear
				},
				ratio: {
					set: setHalfTwoRatio,
					get: getHalfTwoRatio
				}
			});
		}, false);
	}

	//half_3

	let halfThreeInterval;
	const halfThreeIntervalSet = () => {
		halfThreeInterval = setInterval(() => {
			refreshBid({
				elementCode: halfThreeCode, 
				slot: slot6
			})
		}, AD_REFRESH_INTERVAL);
	}
	const halfThreeIntervalClear = () => clearInterval(halfThreeInterval)

	let halfThreeRatio = 0;
	const setHalfThreeRatio = (x) => { halfThreeRatio = x }
	const getHalfThreeRatio = () => { return halfThreeRatio }

	if(halfThree){
		window.addEventListener("load", () => {
			createObserver({
				element: halfThree,
				interval: {
					set: halfThreeIntervalSet,
					clear: halfThreeIntervalClear
				},
				ratio: {
					set: setHalfThreeRatio,
					get: getHalfThreeRatio
				}
			});
		}, false);
	}

	//half_4

	let halfFourInterval;
	const halfFourIntervalSet = () => {
		halfFourInterval = setInterval(() => {
			refreshBid({
				elementCode: halfFourCode, 
				slot: slot7
			})
		}, AD_REFRESH_INTERVAL);
	}
	const halfFourIntervalClear = () => clearInterval(halfFourInterval)

	let halfFourRatio = 0;
	const setHalfFourRatio = (x) => { halfFourRatio = x }
	const getHalfFourRatio = () => { return halfFourRatio }

	if(halfFour){
		window.addEventListener("load", () => {
			createObserver({
				element: halfFour,
				interval: {
					set: halfFourIntervalSet,
					clear: halfFourIntervalClear
				},
				ratio: {
					set: setHalfFourRatio,
					get: getHalfFourRatio
				}
			});
		}, false);
	}

	//rec_1

	let recOneInterval;
	const recOneIntervalSet = () => {
		recOneInterval = setInterval(() => {
			refreshBid({
				elementCode: recOneCode, 
				slot: slot8
			})
		}, AD_REFRESH_INTERVAL);
	}
	const recOneIntervalClear = () => clearInterval(recOneInterval)

	let recOneRatio = 0;
	const setRecOneRatio = (x) => { recOneRatio = x }
	const getRecOneRatio = () => { return recOneRatio }

	if(recOne){
		window.addEventListener("load", () => {
			createObserver({
				element: recOne,
				interval: {
					set: recOneIntervalSet,
					clear: recOneIntervalClear
				},
				ratio: {
					set: setRecOneRatio,
					get: getRecOneRatio
				}
			});
		}, false);
	}

	//rec_2

	let recTwoInterval;
	const recTwoIntervalSet = () => {
		recTwoInterval = setInterval(() => {
			refreshBid({
				elementCode: recTwoCode, 
				slot: slot9
			})
		}, AD_REFRESH_INTERVAL);
	}
	const recTwoIntervalClear = () => clearInterval(recTwoInterval)

	let recTwoRatio = 0;
	const setRecTwoRatio = (x) => { recTwoRatio = x }
	const getRecTwoRatio = () => { return recTwoRatio }

	if(recTwo){
		window.addEventListener("load", () => {
			createObserver({
				element: recTwo,
				interval: {
					set: recTwoIntervalSet,
					clear: recTwoIntervalClear
				},
				ratio: {
					set: setRecTwoRatio,
					get: getRecTwoRatio
				}
			});
		}, false);
	}

	//rec_3

	let recThreeInterval;
	const recThreeIntervalSet = () => {
		recThreeInterval = setInterval(() => {
			refreshBid({
				elementCode: recThreeCode, 
				slot: slot10
			})
		}, AD_REFRESH_INTERVAL);
	}
	const recThreeIntervalClear = () => clearInterval(recThreeInterval)

	let recThreeRatio = 0;
	const setRecThreeRatio = (x) => { recThreeRatio = x }
	const getRecThreeRatio = () => { return recThreeRatio }

	if(recThree){
		window.addEventListener("load", () => {
			createObserver({
				element: recThree,
				interval: {
					set: recThreeIntervalSet,
					clear: recThreeIntervalClear
				},
				ratio: {
					set: setRecThreeRatio,
					get: getRecThreeRatio
				}
			});
		}, false);
	}

	//rec_4

	let recFourInterval;
	const recFourIntervalSet = () => {
		recFourInterval = setInterval(() => {
			refreshBid({
				elementCode: recFourCode, 
				slot: slot11
			})
		}, AD_REFRESH_INTERVAL);
	}
	const recFourIntervalClear = () => clearInterval(recFourInterval)

	let recFourRatio = 0;
	const setRecFourRatio = (x) => { recFourRatio = x }
	const getRecFourRatio = () => { return recFourRatio }

	if(recFour){
		window.addEventListener("load", () => {
			createObserver({
				element: recFour,
				interval: {
					set: recFourIntervalSet,
					clear: recFourIntervalClear
				},
				ratio: {
					set: setRecFourRatio,
					get: getRecFourRatio
				}
			});
		}, false);
	}

	document.addEventListener("visibilitychange", () => {
		if(document.hidden){
			halfStickyIntervalClear()
			billboardOneIntervalClear()
			billboardTwoIntervalClear()
			halfOneIntervalClear()
			halfTwoIntervalClear()
			halfThreeIntervalClear()
			halfFourIntervalClear()
			recOneIntervalClear()
			recTwoIntervalClear()
			recThreeIntervalClear()
			recFourIntervalClear()
		}
	});

	const createVignettes = (g_ads_enabled) => {
		// if(halfSticky) halfSticky.prepend(vignette.cloneNode(true));
		// if(billboardOne) billboardOne.prepend(vignette.cloneNode(true));
		// if(billboardTwo) billboardTwo.prepend(vignette.cloneNode(true));
		// if(halfOne) halfOne.prepend(vignette.cloneNode(true));
		// if(halfTwo) halfTwo.prepend(vignette.cloneNode(true));
		// if(halfThree) halfThree.prepend(vignette.cloneNode(true));
		// if(halfFour) halfFour.prepend(vignette.cloneNode(true));
		// if(recOne) recOne.prepend(vignette.cloneNode(true));
		// if(recTwo) recTwo.prepend(vignette.cloneNode(true));
		// if(recThree) recThree.prepend(vignette.cloneNode(true));
		// if(recFour) recFour.prepend(vignette.cloneNode(true));

		if(g_ads_enabled){
			const firstAutoAd = document.getElementsByClassName('google-auto-placed')[0];
			const secondAutoAd = document.getElementsByClassName('google-auto-placed')[1];

			if(firstAutoAd) firstAutoAd.prepend(vignette.cloneNode(true));
			if(secondAutoAd) secondAutoAd.prepend(vignette.cloneNode(true));
		}
	}

	setTimeout(() => {
		createVignettes(g_ads_enabled);
	}, VIGNETTE_TIMEOUT);
	   
    //biders
    
	const adUnits = [
        {
            code: billboardOneCode,
            mediaTypes: {
                banner: {
                    sizes: [[750, 300],[750, 200],[750, 100],[728, 90]],
                }
            }
            ,
            bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '88430'
					}
				}
				,
				{
					bidder: 'adform',
					params: {
						mid: '565946'
					}
				}
				,
				{
					bidder: 'adform',
					params: {
						mid: '714497'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422, 
						siteId: 145384, 
						pageId: 767412, 
						formatId: 51751
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581, 
						siteId: 162117, 
						pageId: 930021, 
						formatId: 54146
					}
				}
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId: '1624188',
						sizes: '41'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: billboardOneCode,
						placement: billboardOneCode,
						environment: 'desktop'
					}
				}
				,
				{
					bidder: 'sspBC'
				}    
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				// 		placementId: "122108155252256"
				// 	}
				// }   
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'Y8xrbiaN7G',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// }            
			]
        }
        ,
        {
            code: billboardTwoCode,
            mediaTypes: {
                banner: {
                    sizes: [[750, 300],[750, 200],[750, 100],[728, 90]],
                }
            }
            ,
            bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '956084'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422, 
						siteId: 145384, 
						pageId: 767412, 
						formatId: 51751
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 162117,
						pageId: 930021,
						formatId: 54146
					}
				}
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId: '1624188',
						sizes: '41'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: billboardTwoCode,
						placement: billboardTwoCode,
						environment: 'desktop'
					}
				}   
				,
				{
					bidder: 'sspBC'
				}  
				// ,
				// {
				// 	bidder: 'adpone',
				// 	params: {
				// 		placementId: "122108155252256"
				// 	}
				// }  
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'Y8xrbiaN7G',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// }                           
			]
        }
        ,        
        {
            code: halfStickyCode,
            mediaTypes: {
                banner: {
                    sizes: [[300, 600]],
                }
            }
            ,
            bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '114466'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 145384,
						pageId: 818285,
						formatId: 65479
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 162117,
						pageId: 1196412,
						formatId: 55370
					}
				}
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu', 
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId: '1624190',
						sizes: '10',
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: halfStickyCode,
						placement: halfStickyCode,
						environment: 'desktop'
					}
				} 
				,
				{
					bidder: 'sspBC'
				}  
				// ,
				// {
				// 	bidder: 'adpone',
				// 	params: {
				// 		placementId: "122108155250520"
				// 	}
				// }   
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'uPauI6Vbfe',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// }        
			]
        }
        ,
        {
            code: halfOneCode,
            mediaTypes: {
                banner: {
                    sizes: [[300, 600]],
                }
            }
            ,
            bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '956104'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu', 
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422, 
						siteId: 145384, 
						pageId: 818285, 
						formatId: 65479
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581, 
						siteId: 162117, 
						pageId: 898109, 
						formatId: 79477
					}
				}    
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047', 
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId: '1624190',
						sizes: '10'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: halfOneCode,
						placement: halfOneCode,
						environment: 'desktop'
					}
				} 
				,
				{
					bidder: 'sspBC'
				}
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				//         placementId: "122108155250520"
				//     }
				// }   
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'uPauI6Vbfe',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// }                 
			]
		}
		,
		{
			code: halfTwoCode,
			mediaTypes: {
				banner: {
					sizes: [[300, 600]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '956105'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu', 
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 145384,
						pageId: 818285,
						formatId: 65479
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 162117,
						pageId: 817843,
						formatId: 55808
					}
				}
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047', 
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId: '1624190',
						sizes: '10'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: halfTwoCode,
						placement: halfTwoCode,
						environment: 'desktop'
					}
				}    
				,
				{
					bidder: 'sspBC'
				} 
				// ,
				// {
				//     bidder: 'adpone',
				//     params:{
				//         placementId: "122108155250520"
				//     }
				// }  
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'uPauI6Vbfe',
						supplyType: "site"
				    }
				}
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// }               
            ]
        }
        ,
        {
            code: halfThreeCode,
            mediaTypes: {
                banner: {
                    sizes: [[300, 600]],
                }
            }
            ,
            bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '956106'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu', 
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 145384,
						pageId: 818285,
						formatId: 65479
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 162117,
						pageId: 1600280,
						formatId: 68948
					}
				}
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId: '1624190',
						sizes: '10',
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				} 
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: halfThreeCode,
						placement: halfThreeCode,
						environment: 'desktop'
					}
				}  
				,
				{
                	bidder: 'sspBC'
                }  
				,
				{
				    bidder: 'adpone',
				    params: {
				        placementId: "122108155250520"
				    }
				} 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'uPauI6Vbfe',
						supplyType: "site"
				    }
				} 
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// } 
            ]
        }
        ,
        {
            code: halfFourCode,
            mediaTypes: {
                banner: {
                    sizes: [[300, 600]],
                }
            }
            ,
            bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '956107'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 145384,
						pageId: 818285,
						formatId: 65479
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 162117,
						pageId: 1600281,
						formatId: 79476
					}
				}
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId: '1624190',
						sizes: '10'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: halfFourCode,
						placement: halfFourCode,
						environment: 'desktop'
					}
				}  
				,
				{
					bidder: 'sspBC'
				} 
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				//         placementId: "122108155250520"
				//     }
				// } 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'uPauI6Vbfe',
						supplyType: "site"
				    }
				}  
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// }                
			]
		}
		,
		{
			code: recOneCode,
			mediaTypes: {
				banner: {
					sizes: [[336, 280], [300, 250]],
				}
			}
			,
			bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '179562'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 145384,
						pageId: 770176,
						formatId: 52161
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 162117,
						pageId: 996330,
						formatId: 54306
					}
				}    
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId: '1624184',
						sizes: '16'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: recOneCode,
						placement: recOneCode,
						environment: 'desktop'
					}
				}   
				,
				{
                	bidder: 'sspBC'
                } 
				// ,
				// {
				// 	bidder: 'adpone',
				// 	params: {
				// 		placementId: "122108155248577"
				// 	}
				// }  
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'mubo8g7fqZ',
						supplyType: "site"
				    }
				}  
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// }             
            ]
        }
        ,
        {
            code: recTwoCode,
            mediaTypes: {
                banner: {
                    sizes: [[336, 280], [300, 250]],
                }
            }
            ,
            bids: [
				{
					bidder: 'adform',
					params: {
						mid: '179562'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 145384,
						pageId: 770176,
						formatId: 52161
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 162117,
						pageId: 996331,
						formatId: 54307
					}
				}    
		
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId: '1624184',
						sizes: '16'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: recTwoCode,
						placement: recTwoCode,
						environment: 'desktop'
					}
				}  
				,
				{
					bidder: 'sspBC'
				}   
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				//         placementId: "122108155248577"
				//     }
				// } 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'mubo8g7fqZ',
						supplyType: "site"
				    }
				}  
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// }              
            ]
        }
        ,
        {
            code: recThreeCode,
            mediaTypes: {
                banner: {
                    sizes: [[336, 280], [300, 250]],
                }
            }
            ,
            bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '241023'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu', 
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 145384,
						pageId: 770177,
						formatId: 52161
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 162117,
						pageId: 996332,
						formatId: 54308
					}
				}
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId: '1624184',
						sizes: '16'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: recThreeCode,
						placement: recThreeCode,
						environment: 'desktop'
					}
				}  
				,
				{
					bidder: 'sspBC'
				} 
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				//         placementId: "122108155248577"
				//     }
				// } 
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'mubo8g7fqZ',
						supplyType: "site"
				    }
				}   
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// }                 
            ]
        }
        ,
        {
            code: recFourCode,
            mediaTypes: {
                banner: {
                    sizes: [[336, 280], [300, 250]],
                }
            }
            ,
            bids: [ 
				{
					bidder: 'adform',
					params: {
						mid: '241023'
					}
				}
				,
				{
					bidder: "rtbhouse",
					params: {
						region: 'prebid-eu',
						publisherId: 'ubZbp6DokIAsAJBBqd3T'
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2422,
						siteId: 145384,
						pageId: 770177,
						formatId: 52161
					}
				}
				,
				{
					bidder: 'smartadserver',
					params: {
						networkId: 2581,
						siteId: 162117,
						pageId: 1600287,
						formatId: 54309
					}
				}    
				,
				{
					bidder: 'oftmedia',
					params: {
						placementId: 21068619
					}
				}
				,
				{
					bidder: 'criteo',
					params: {
						networkId: 7049
					}
				}
				// ,
				// {
				// 	bidder: 'connectad',
				// 	params: {
				// 		networkId: '10047',
				// 		siteId: '1033590'
				// 	}
				// }
				,
				{
					bidder: 'rubicon',
					params: {
						accountId: '21594',
						siteId: '316854',
						zoneId: '1624184',
						sizes: '16'
					}
				}
				,
				{
					bidder: "amx",
					params: {
						memberId: "152media"
					}
				}
				,
				{
					bidder: 'adagio',
					params: {
						organizationId: '1120',
						site: 'mistrzowie-org',
						adUnitElementId: recFourCode,
						placement: recFourCode,
						environment: 'desktop'
					}
				}   
				,
				{
					bidder: 'sspBC'
				}  
				// ,
				// {
				//     bidder: 'adpone',
				//     params: {
				//         placementId: "122108155248577"
				//     }
				// }  
				,
				{
				    bidder: 'richaudience',
				    params: {
				        pid: 'mubo8g7fqZ',
						supplyType: "site"
				    }
				}   
				// ,
				// {
				// 	bidder: 'smilewanted',
				// 	params: {
				// 		zoneId: 'yieldriser.pl_hb_display',
				// 		bidfloor: 0.00
				// 	}
				// }              
            ]
        }    
    ];

	pbjs = pbjs || {};
	pbjs.que = pbjs.que || [];

	pbjs.que.push(function() {
		pbjs.addAdUnits(adUnits);
		pbjs.setConfig({
			userSync: {
				syncEnabled: true,
				filterSettings: {        
					all: {        
						bidders: "*",        
						filter: "include"
					}        
				},        
				syncsPerBidder: 5,        
				syncDelay: 3000,        
				auctionDelay: 0,        
				userIds: [
					{                
						name: "criteo",
					}
				]
			},
			schain: { 
				validation: "strict", 
				config: { 
					ver: "1.0", 
					complete: 1, 
					nodes: [
						{ 
							asi: "yieldriser.com", 
							sid: "27", 
							hp: 1 
						}
					]
				}
			},
			bidderSequence: "random",
			disableAjaxTimeout: true,
			consentManagement: {
				cmpApi: 'iab',
				timeout: 5000,
				allowAuctionWithoutConsent: true
			},
			criteo: {
				storageAllowed: true, // opt-in to allow Criteo bid adapter to access the storage
			},
			currency: {
				adServerCurrency: "PLN",
			 }
		});
		pbjs.requestBids({
			bidsBackHandler: initAdserver,
			timeout: PREBID_TIMEOUT
		});
	});
}