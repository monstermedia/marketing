if(MMG.isAdblockDisabled()){
	
	var googletag = null;
	var pbjs = null;

	const PREBID_TIMEOUT = g_ads_enabled ? 1000 : 1200;
	const VIGNETTE_TIMEOUT = 5000;

	//div containers

	const topBill = document.getElementById('top_bill');
	const topBillInnerCode = 'top_bill_inner';
	const topBillInner = document.getElementById(topBillInnerCode);

	const topRect = document.getElementById('top_rect');
	const topRectInnerCode = 'top_rect_inner';
	const topRectInner = document.getElementById(topRectInnerCode);

	const bottomRect = document.getElementById('bottom_rect');
	const bottomRectInnerCode = 'bottom_rect_inner';
	const bottomRectInner = document.getElementById(bottomRectInnerCode);
	
	const vignette = document.createElement('div');
	vignette.style.textAlign = 'center';
	vignette.style.color = 'grey';
	vignette.style.fontSize = '10px';
	vignette.innerHTML = 'Reklama';

	//google ad slots

	const GPTScript = document.createElement('script');
	GPTScript.async = true;
	GPTScript.setAttribute('src', '//securepubads.g.doubleclick.net/tag/js/gpt.js');
	top.document.head.appendChild(GPTScript);

	window.googletag = window.googletag || { cmd: [] };

	googletag.cmd.push(function() {
		//if(topBillInner){
			googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3845284974/demotywatory.pl_top_bill_mobile', [[336, 280],[300, 250]], topBillInnerCode) //top_bill_inner
				.addService(googletag.pubads());
		//}
		//if(topRectInner){
			googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3845284974/demotywatory.pl_top_rect', [[336, 280],[300, 250]], topRectInnerCode) //top_rect_inner
				.addService(googletag.pubads());    
		//}
		//if(bottomRectInner){
			googletag.defineSlot('/1004243/ca-pub-9977706094520851-tag/3845284974/demotywatory.pl_bottom_rect', [[336, 280],[300, 250]], bottomRectInnerCode) //bottom_rect_inner
				.addService(googletag.pubads());
		//}

		googletag.pubads().enableSingleRequest();
		googletag.pubads().disableInitialLoad();
		googletag.pubads().collapseEmptyDivs();
		googletag.pubads().setForceSafeFrame(false);
		googletag.pubads().setCentering(true);
		googletag.enableServices();
	});

	googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];

	//prebid currencies

	pbjs = pbjs || {};
	pbjs.que = pbjs.que || [];
		
	// fetch('https://prebid-config-open-data.s3.eu-central-1.amazonaws.com/currencies')
    // .then(resp => resp.json())
    // .then(data => {
    //     console.log("Aktualne kursy walut: ", data);
         
    //     const USD = data.usd.rate;
    //     const EUR = data.eur.rate;
    
        pbjs.bidderSettings = {
			criteo: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
				,
				storageAllowed: true
			}
			,
			adform: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.9;
				}
			}
			,
			smartadserver: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			rtbhouse: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			pulsepoint: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			onedisplay: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.85;
				}
			}
			,
			oftmedia: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.85;
				}
			}
			,
			imonomy: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			rubicon: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm*0.75;
				}
			}
			,
			ix: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			// ,
			// connectad: {
			// 	bidCpmAdjustment: function(bidCpm) {
			// 		return bidCpm;
			// 	}
			// }
			,
			amx: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			visx: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				}
			}
			,
			adagio: {
				bidCpmAdjustment: function(bidCpm) {
					return bidCpm;
				},
				storageAllowed: true 
			}
			,
			standard: {
				adserverTargeting: [ 
					{
						key: "hb_bidder",
						val: function (bidResponse) {
							return bidResponse.bidderCode;
						}
					}
					,
					{
						key: "hb_adid",
						val: function (bidResponse) {
							return bidResponse.adId;
						}
					}
					,
					{
						key: "hb_pb",
						val: function(bidResponse) {
							const cpm = bidResponse.cpm;
							if (cpm < 10.00) {
								return (Math.floor(cpm * 100) / 100).toFixed(2);
							}
							else {
								return '10.00';
							}
						}
					}
				]
			}
		};
    // });

	//prebid initialization function

	function initAdserver() {
		if (pbjs.initAdserverSet) return;
		pbjs.initAdserverSet = true;
		googletag.cmd.push(function() {
			pbjs.que.push(function() {
				pbjs.setTargetingForGPTAsync();
				googletag.pubads().refresh();
			});
		});
	}

	const createVignettes = (g_ads_enabled) => {
		//if(topBill) topBill.prepend(vignette.cloneNode(true));
		//if(topRect) topRect.prepend(vignette.cloneNode(true));
		//if(bottomRect) bottomRect.prepend(vignette.cloneNode(true));
		
		document.getElementById('top_bill').prepend(vignette.cloneNode(true));
		document.getElementById('top_rect').prepend(vignette.cloneNode(true));
		document.getElementById('bottom_rect').prepend(vignette.cloneNode(true));

		if(g_ads_enabled){
			const firstAutoAd = document.getElementsByClassName('google-auto-placed')[0];
			const secondAutoAd = document.getElementsByClassName('google-auto-placed')[1];
			const thirdAutoAd = document.getElementsByClassName('google-auto-placed')[2];
			const fourthAutoAd = document.getElementsByClassName('google-auto-placed')[3];
			const fifthAutoAd = document.getElementsByClassName('google-auto-placed')[4];
			const sixthAutoAd = document.getElementsByClassName('google-auto-placed')[5];

			if(firstAutoAd) firstAutoAd.prepend(vignette.cloneNode(true));
			if(secondAutoAd) secondAutoAd.prepend(vignette.cloneNode(true));
			if(thirdAutoAd) thirdAutoAd.prepend(vignette.cloneNode(true));
			if(fourthAutoAd) fourthAutoAd.prepend(vignette.cloneNode(true));
			if(fifthAutoAd) fifthAutoAd.prepend(vignette.cloneNode(true));
			if(sixthAutoAd) sixthAutoAd.prepend(vignette.cloneNode(true));
		}
	}

	//separate configs for autoads enabled and disabled

	if(g_ads_enabled){

		const AAScript = document.createElement('script');
		AAScript.async = true;
		AAScript.setAttribute('data-ad-client', 'ca-pub-9948870393389755');
		AAScript.setAttribute('src', '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');
		top.document.head.appendChild(AAScript);

		setTimeout(() => {
			createVignettes(g_ads_enabled);
		}, VIGNETTE_TIMEOUT);

		//biders

		const adUnits = [
			{
				code: topBillInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[300, 250],[336, 280]],
					}
				}
				,
				bids: [
					{
						bidder: 'adform',
						params: {
							mid: '1305468'
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId: 170496, pageId: 840781, formatId: 52161,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId: 167130, pageId: 907607, formatId: 54302,
						}
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281136',
							sizes:'16',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1418180,
							publisherSubId: topBillInnerCode
						}
					}
					,
					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params: {
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: topBillInnerCode,
							placement: topBillInnerCode,
							environment: 'mobile',
						}
					}
					,
					{
						bidder: 'adpone',
						params: {
							placementId: "122108154259386"
						}
					} 
					,
					{
						bidder: 'richaudience',
						params: {
							pid: 'LqlUbjXh7y',
							supplyType: "site"
						}
					} 
					// ,
					// {
					// 	bidder: 'smilewanted',
					// 	params: {
					// 		zoneId: 'yieldriser.pl_hb_display',
					// 		bidfloor: 0.00
					// 	}
					// } 

				]
			}
			,
			{
				code: topRectInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[300, 250],[336, 280]],
					}
				}
				,
				bids: [
					{
						bidder: 'adform',
						params: {
							mid: '344424'
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId:  170496, pageId: 840780, formatId: 62779,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId:  167130, pageId: 907610, formatId: 54303,
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281136',
							sizes:'16',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1418180,
							publisherSubId: topRectInnerCode
						}
					}
					,
					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params: {
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: topRectInnerCode,
							placement: topRectInnerCode,
							environment: 'mobile',
						}
					}
					,
					{
						bidder: 'adpone',
						params: {
							placementId: "122108154259386"
						}
					}
					,
					{
						bidder: 'richaudience',
						params: {
							pid: 'LqlUbjXh7y',
							supplyType: "site"
						}
					} 
					// ,
					// {
					// 	bidder: 'smilewanted',
					// 	params: {
					// 		zoneId: 'yieldriser.pl_hb_display',
					// 		bidfloor: 0.00
					// 	}
					// }  

				]
			}
			,
			{
				code: bottomRectInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[300, 250],[336, 280]],
					}
				}
				,
				bids: [ 
					{
						bidder: 'adform',
						params: {
							mid: '344427'
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId:  154887, pageId: 792814, formatId: 52161,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId:  167130, pageId: 1045260, formatId: 54306,
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281136',
							sizes:'16',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1418180,
							publisherSubId: bottomRectInnerCode
						}
					}
					,
					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params: {
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: bottomRectInnerCode,
							placement: bottomRectInnerCode,
							environment: 'mobile',
						}
					}
					,
					{
						bidder: 'adpone',
						params: {
							placementId: "122108154259386"
						}
					} 
					,
					{
						bidder: 'richaudience',
						params: {
							pid: 'LqlUbjXh7y',
							supplyType: "site"
						}
					} 
					// ,
					// {
					// 	bidder: 'smilewanted',
					// 	params: {
					// 		zoneId: 'yieldriser.pl_hb_display',
					// 		bidfloor: 0.00
					// 	}
					// } 
				]
			}
		];

		pbjs = pbjs || {};
		pbjs.que = pbjs.que || [];

		pbjs.que.push(function() {
			pbjs.addAdUnits(adUnits);
			pbjs.setConfig({
				userSync: {
					syncEnabled: true,
					filterSettings: {        
						all: {        
							bidders: "*",        
							filter: "include"
						}        
					},        
					syncsPerBidder: 5,        
					syncDelay: 3000,        
					auctionDelay: 0,        
					userIds: [
						{                
							name: "criteo",
						}
					]
				},
				schain: { 
					validation: "strict", 
					config: { 
						ver: "1.0", 
						complete: 1, 
						nodes: [
							{ 
								asi: "yieldriser.com", 
								sid: "27", 
								hp: 1 
							}
						]
					}
				},
				bidderSequence: "random",
				disableAjaxTimeout: true,
				consentManagement: {
					cmpApi: 'iab',
					timeout: 5000,
					allowAuctionWithoutConsent: true
				}
			});
			pbjs.requestBids({
				bidsBackHandler: initAdserver,
				timeout: PREBID_TIMEOUT
			});
		});

	} else {

		setTimeout(() => {
			createVignettes(g_ads_enabled);
		}, VIGNETTE_TIMEOUT);

		//biders
		
		const adUnits = [
			{
				code: topBillInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[300, 250],[336, 280]],
					}
				}
				,
				bids: [
					{
						bidder: 'adform',
						params: {
							mid: '1428074'
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId: 170496, pageId: 840781, formatId: 52161,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId: 167130, pageId: 907607, formatId: 54302,
						}
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281136',
							sizes:'16',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1418180,
							publisherSubId: topBillInnerCode
						}
					}
					,
					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params: {
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: topBillInnerCode,
							placement: topBillInnerCode,
							environment: 'mobile',
						}
					}
					,
					{
						bidder: 'adpone',
						params: {
							placementId: "122108154259386"
						}
					} 
					,
					{
						bidder: 'richaudience',
						params: {
							pid: 'LqlUbjXh7y',
							supplyType: "site"
						}
					} 
				]
			}
			,
			{
				code: topRectInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[300, 250],[336, 280]],
					}
				}
				,
				bids: [ 
					{
						bidder: 'adform',
						params: {
							mid: '1428072'
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId:  170496, pageId: 840780, formatId: 62779,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId:  167130, pageId: 907610, formatId: 54303,
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281136',
							sizes:'16',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1418180,
							publisherSubId: topRectInnerCode
						}
					}
					,
					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params: {
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: topRectInnerCode,
							placement: topRectInnerCode,
							environment: 'mobile',
						}
					}
					,
					{
						bidder: 'adpone',
						params: {
							placementId: "122108154259386"
						}
					} 
					,
					{
						bidder: 'richaudience',
						params: {
							pid: 'LqlUbjXh7y',
							supplyType: "site"
						}
					} 
				]
			}
			,
			{
				code: bottomRectInnerCode,
				mediaTypes: {
					banner: {
						sizes: [[300, 250],[336, 280]],
					}
				}
				,
				bids: [
					{
						bidder: 'adform',
						params: {
							mid: '1428062'  
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2422, siteId:  154887, pageId: 792814, formatId: 52161,
						}
					}
					,
					{
						bidder: 'smartadserver',
						params: {
							networkId: 2581, siteId:  167130, pageId: 1045260, formatId: 54306,
						}
					}
					,
					{
						bidder: 'sspBC'
					}
					,
					{
						bidder: 'rubicon',
						params: {
							accountId: '21594',
							siteId: '406510',
							zoneId:'2281136',
							sizes:'16',
						}
					}
					,
					{
						bidder: 'criteo',
						params: {
							zoneId: 1418180,
							publisherSubId: bottomRectInnerCode
						}
					}
					,
					{
						bidder: "rtbhouse",
						params: {
							region: 'prebid-eu',
							publisherId: 'ubZbp6DokIAsAJBBqd3T'
						}
					}
					,
					{
						bidder: 'oftmedia',
						params: {
							placementId: 23981220
						}
					}
					// ,
					// {
					// 	bidder: 'connectad',
					// 	params: {
					// 		networkId: '10047',
					// 		siteId: '1055768'
					// 	}
					// }
					,
					{
						bidder: 'adagio',
						params: {
							organizationId: '1120',
							site: 'demotywatory-pl',
							adUnitElementId: bottomRectInnerCode,
							placement: bottomRectInnerCode,
							environment: 'mobile',
						}
					}
					,
					{
						bidder: 'adpone',
						params: {
							placementId: "122108154259386"
						}
					} 
					,
					{
						bidder: 'richaudience',
						params: {
							pid: 'LqlUbjXh7y',
							supplyType: "site"
						}
					} 
				]
			}
		];

		pbjs = pbjs || {};
		pbjs.que = pbjs.que || [];

		pbjs.que.push(function() {
			pbjs.addAdUnits(adUnits);
			pbjs.setConfig({
				userSync: {
					userIds: [{
						name: "criteo",
					}]
				},
				schain: { 
					validation: "strict", 
					config: { 
						ver: "1.0", 
						complete: 1, 
						nodes: [ 
							{ 
								asi: "yieldriser.com", 
								sid: "27", 
								hp: 1
							}
						]
					}
				},
				bidderSequence: "random",
				disableAjaxTimeout: true,
				consentManagement: {
					cmpApi: 'iab',
					timeout: 5000,
					allowAuctionWithoutConsent: true
				},
				criteo: {
					storageAllowed: true, // opt-in to allow Criteo bid adapter to access the storage
				},
				currency: {
					adServerCurrency: "PLN",
				}
				
			});
			pbjs.requestBids({
				bidsBackHandler: initAdserver,
				timeout: PREBID_TIMEOUT
			});
		});
	}
}